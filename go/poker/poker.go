/* My solution
package poker

import (
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strings"
)

//create a combination type
type Combination int

//map to order card
var cardMap = map[string]int{
	"2":  2,
	"3":  3,
	"4":  4,
	"5":  5,
	"6":  6,
	"7":  7,
	"8":  8,
	"9":  9,
	"10": 10,
	"J":  11,
	"Q":  12,
	"K":  13,
	"A":  14,
}

//map to reverse card
var reversedCardMap = map[int]string{
	2:  "2",
	3:  "3",
	4:  "4",
	5:  "5",
	6:  "6",
	7:  "7",
	8:  "8",
	9:  "9",
	10: "10",
	11: "J",
	12: "Q",
	13: "K",
	14: "A",
}

//create a card type
type Card struct {
	value int
	sign  string
}

//create a hand type to compare hand
type Hand struct {
	cards           []*Card
	input           string
	bestCombination Combination
	bestValue       int
	secbestValue    int
	firstPair       int
	secondPair      int
	bestThree       int
	bestFour        int
}

//CardString enables to print a Card
func (c *Card) CardString() string {
	return fmt.Sprintf("%s%s", reversedCardMap[c.value], c.sign)
}

//BestHand find the best hand in a list of hand
func BestHand(hands []string) (resultHand []string, err error) {

	//check if the input is empty
	if len(hands) == 0 {
		return nil, errors.New("empty hand")
	}

	//create a list of hand
	list_h := make([]*Hand, len(hands))

	for i := 0; i < len(hands); i++ {

		c, err := CardHand(hands[i])

		if err != nil {
			return nil, err
		} else {
			list_h[i] = c
		}
	}

	//compare the best combination for every hand
	var bestH = Hand{make([]*Card, 5), "", 11, 0, 0, 0, 0, 0, 0} //make([]*Card, 5)

	for _, el := range list_h {

		switch {
		case el.bestCombination < bestH.bestCombination:
			bestH = *el
			resultHand = nil
		case el.bestCombination == bestH.bestCombination:
			switch bestH.bestCombination {
			case 10:
				switch {
				case bestH.cards[4].value < el.cards[4].value,
					bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value < el.cards[3].value,
					bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value == el.cards[3].value && bestH.cards[2].value < el.cards[2].value,
					bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value == el.cards[3].value && bestH.cards[2].value == el.cards[2].value && bestH.cards[1].value < el.cards[1].value,
					bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value == el.cards[3].value && bestH.cards[2].value == el.cards[2].value && bestH.cards[1].value < el.cards[1].value,
					bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value == el.cards[3].value && bestH.cards[2].value == el.cards[2].value && bestH.cards[1].value == el.cards[1].value && bestH.cards[0].value < el.cards[0].value:
					bestH = *el
					resultHand = nil
				case bestH.cards[4].value == el.cards[4].value && bestH.cards[3].value == el.cards[3].value && bestH.cards[2].value == el.cards[2].value && bestH.cards[1].value == el.cards[1].value && bestH.cards[0].value == el.cards[0].value:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 9:
				//TODO
				if el.firstPair > bestH.firstPair {
					bestH = *el
					resultHand = nil
				}

			case 8:
				switch {
				case el.firstPair > bestH.firstPair,
					el.firstPair == bestH.firstPair && el.secondPair > bestH.secondPair,
					el.firstPair == bestH.firstPair && el.secondPair == bestH.secondPair && el.bestValue > bestH.bestValue:
					bestH = *el
					resultHand = nil
				case el.firstPair == bestH.firstPair && el.secondPair == bestH.secondPair && el.bestValue == bestH.bestValue:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 7:
				switch {
				case el.bestThree > bestH.bestThree,
					el.bestThree == bestH.bestThree && el.bestValue > bestH.bestValue,
					el.bestThree == bestH.bestThree && el.bestValue == bestH.bestValue && el.secbestValue > bestH.secbestValue:
					bestH = *el
					resultHand = nil
				case el.bestThree == bestH.bestThree && el.bestValue == bestH.bestValue && el.secbestValue == bestH.secbestValue:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 6:
				switch {
				case el.cards[4].value > bestH.cards[4].value:
					bestH = *el
					resultHand = nil
				case el.cards[4].value == bestH.cards[4].value:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 5:
				switch {
				case el.cards[4].value > bestH.cards[4].value:
					bestH = *el
					resultHand = nil
				case el.cards[4].value == bestH.cards[4].value:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 4:
				switch {
				case el.bestThree > bestH.bestThree,
					el.bestThree == bestH.bestThree && el.firstPair > bestH.firstPair:
					bestH = *el
					resultHand = nil
				case el.bestThree == bestH.bestThree && el.firstPair == bestH.firstPair:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 3:

				switch {
				case el.bestFour > bestH.bestFour,
					el.bestFour == bestH.bestFour && el.bestValue > bestH.bestValue:
					bestH = *el
					resultHand = nil
				case el.bestFour == bestH.bestFour && el.bestValue == bestH.bestValue:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			case 2:
				switch {
				case el.cards[4].value > bestH.cards[4].value:
					bestH = *el
					resultHand = nil
				case el.cards[4].value == bestH.cards[4].value:
					resultHand = append(resultHand, bestH.input)
					bestH = *el
				}

			}
		}
	}

	resultHand = append(resultHand, bestH.input)

	return resultHand, nil
}

//CardHand enables to create a hand struct
func CardHand(s string) (*Hand, error) {

	//parse string to create new cards
	l_cards := strings.Split(s, " ")
	re_value := regexp.MustCompile("1[0]|[1-9]|J|K|Q|A")
	re_sign := regexp.MustCompile("♡♡|♤|♢|♧|♡")

	//check input
	if len(l_cards) != 5 {
		return nil, errors.New("the hand hasn't the good number of cards")
	}

	//create a hand
	var h = new(Hand)
	h.input = s

	for _, el := range l_cards {
		var c = new(Card)

		//add value , check and sign to c
		s := re_sign.FindString(el)
		v := re_value.FindString(el)

		switch v {
		case "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "K", "Q", "A":
			c.value = cardMap[v]
			h.cards = append(h.cards, c)
		default:
			return h, errors.New("wrong value of card")
		}

		switch s {
		case "♤", "♢", "♧", "♡":
			c.sign = s
		default:
			return h, errors.New("wrong sign of card")
		}

	}

	//sort card in a hand
	sort.Slice(h.cards, func(i, j int) bool {
		return h.cards[i].value < h.cards[j].value
	})

	//find the best combination in the hand
	h.BestCombination()

	return h, nil
}

//BestCombination finds the best combination with a hand
func (h *Hand) BestCombination() {

	//check flush combination
	if h.CheckFlush() {

		switch {
		case h.CheckStraight() && h.cards[0].value == 10: //check royalFlush
			h.bestCombination = 1
		case h.CheckStraight() && h.cards[0].value != 10: //check straightFlush
			h.bestCombination = 2
		default:
			h.bestValue = h.cards[4].value //flush
			h.bestCombination = 5
		}

	} else {

		switch {
		case h.CheckFourOfkind(): //check four of a kind
			h.bestCombination = 3
		case h.CheckFullHouse(): //check full house
			h.bestCombination = 4
		case h.CheckStraight():
			h.bestValue = h.cards[4].value
			h.bestCombination = 6
		case h.CheckThreeOfKind():
			h.bestCombination = 7
		case h.CheckTwoPair():
			h.bestCombination = 8
		case h.CheckPair():
			h.bestCombination = 9

		default:
			//fmt.Println("high card")
			h.bestValue = h.cards[4].value
			h.bestCombination = 10
		}
	}
}

//CheckFlush checks if a hand contain a flush
func (h *Hand) CheckFlush() bool {

	sign := h.cards[0].sign

	for i := 1; i < len(h.cards); i++ {
		if h.cards[i].sign != sign {
			return false
		}

	}

	return true
}

//CheckStraight checks if a hand contain a straight
func (h *Hand) CheckStraight() bool {

	first := h.cards[0].value

	if h.cards[4].value != 14 {
		for i := 1; i < len(h.cards); i++ {
			if h.cards[i].value != first+1 {
				return false
			}

			first++
		}

		return true

	} else {
		if first == 2 || h.cards[3].value == 13 {
			for i := 1; i < len(h.cards)-1; i++ {
				if h.cards[i].value != first+1 {
					return false
				}

				first++
			}

			if h.cards[0].value == 2 {
				h.cards[4].value = 1

				sort.Slice(h.cards, func(i, j int) bool {
					return h.cards[i].value < h.cards[j].value
				})

			}

			return true

		}

		return false

	}

}

//CheckFourOfkind checks if a hand contain a four of a kind
func (h *Hand) CheckFourOfkind() bool {

	//create a map with the value of cards
	value_map := make(map[int]int)
	for _, card := range h.cards {
		value_map[card.value]++
	}

	for id, v := range value_map {
		if v == 4 {
			h.bestFour = id
		} else {
			h.bestValue = id
		}
	}

	return h.bestFour != 0
}

//CheckFullHouse checks if a hand contain a full house
func (h *Hand) CheckFullHouse() bool {

	three := false
	pair := false

	//create a map with the value of cards
	value_map := make(map[int]int)
	for _, card := range h.cards {
		value_map[card.value]++
	}

	for id, v := range value_map {

		if v == 3 {
			three = true
			h.bestThree = id
		}
		if v == 2 {
			pair = true
			h.firstPair = id
		}

	}

	return three && pair
}

//CheckThreeOfKind checks if a hand contain a three of a kind
func (h *Hand) CheckThreeOfKind() bool {
	//create a map with the value of cards
	value_map := make(map[int]int)
	firstValue := 0
	secondValue := 0

	for _, card := range h.cards {
		value_map[card.value]++
	}

	for id, v := range value_map {
		if v == 3 {
			h.bestThree = id
		} else {
			if firstValue == 0 {
				firstValue = id
			} else {
				secondValue = id
			}
		}
	}

	if h.bestThree != 0 {
		if firstValue > secondValue {
			h.bestValue = firstValue
			h.secbestValue = secondValue
		} else {
			h.bestValue = secondValue
			h.secbestValue = firstValue
		}

		return true
	}

	return false
}

//CheckTwoPair checks if a hand contain a two pair
func (h *Hand) CheckTwoPair() bool {
	count_pair := 0
	first_pair := 0
	second_pair := 0
	soloCard := 0
	value_map := make(map[int]int) //create a map with the value of cards

	for _, card := range h.cards {
		value_map[card.value]++
	}

	for id, v := range value_map {
		if v == 2 {
			if count_pair == 0 {
				first_pair = id
			} else {
				second_pair = id
			}
			count_pair++
		} else {
			soloCard = id
		}
	}

	if count_pair == 2 {
		if first_pair > second_pair {
			h.firstPair = first_pair
			h.secondPair = second_pair
		} else {
			h.firstPair = second_pair
			h.secondPair = first_pair
		}
		h.bestValue = soloCard
	}

	return count_pair == 2
}

//CheckPair checks if a hand contain a pair
func (h *Hand) CheckPair() bool {
	//create a map with the value of cards
	value_map := make(map[int]int)
	for _, card := range h.cards {
		value_map[card.value]++
	}

	for id, v := range value_map {
		if v == 2 {
			h.firstPair = id
			return true
		}
	}

	return false
}
*/

//Better solution
package poker

import (
	"errors"
	"sort"
	"strings"
)

type Card struct {
	rank int
	suit rune
}

type Rank struct {
	cat   Category
	count map[int]int
}

type Hand struct {
	cards []Card
	rank  Rank
}

// The card's rank is mapped to the index position in this slice
var cardRanks = [13]string{"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"}

const HighestRank = len(cardRanks) - 1
const (
	Hearts   rune = '♡'
	Spades   rune = '♤'
	Diamonds rune = '♢'
	Clubs    rune = '♧'
)

type Category int

const (
	HighCard Category = iota
	OnePair
	TwoPairs
	ThreeOfAKind
	Straight
	Flush
	FullHouse
	FourOfAKind
	StraightFlush
)

// BestHand picks the best hand(s) from a list of poker hands.
func BestHand(hands []string) (result []string, err error) {

	best := Hand{nil, Rank{-1, nil}}

	for i, s := range hands {

		hand, err := newHand(s, i)

		if err != nil {
			return nil, err
		}

		if hand.rank.cat > best.rank.cat {
			result = []string{s}
			best = hand
		} else if hand.rank.cat == best.rank.cat {

			switch evaluateTie(hand, best) {
			case 1: // hand beats best
				result = []string{s}
				best = hand

			case 0: // unresolved tie
				result = append(result, s)

			case -1: //hand loses to best, do nothing
			}
		}
	}

	return result, nil
}

func newHand(s string, idx int) (Hand, error) {

	h := strings.Split(s, " ")

	if len(h) != 5 {
		return Hand{}, errors.New("invalid number of cards")
	}

	var cards []Card

	for _, c := range h {

		if len(c) < 4 || len(c) > 5 {
			return Hand{}, errors.New("invalid rank-suit combination")
		}

		rank, err := validateRank(c)

		if err != nil {
			return Hand{}, err
		}

		suit, err := validateSuit(c)

		if err != nil {
			return Hand{}, err
		}

		cards = append(cards, Card{rank, suit})

	}

	sort.SliceStable(cards, func(i, j int) bool { return cards[i].rank < cards[j].rank })

	return Hand{cards, rankHand(cards)}, nil
}

func validateRank(s string) (int, error) {

	rank := s[0:1]

	if len(s) == 5 {
		rank = s[0:2]
	}

	for i, cr := range cardRanks {
		if rank == cr {
			return i, nil
		}
	}

	return 0, errors.New("invalid rank")
}

func validateSuit(s string) (rune, error) {

	suit := []rune(s[1:])

	if len(s) == 5 {
		suit = []rune(s[2:])
	}

	switch suit[0] {
	case Hearts, Spades, Diamonds, Clubs:
		return suit[0], nil
	default:
		return 0, errors.New("invalid suit")
	}
}

func rankHand(cards []Card) (rank Rank) {
	// If the high card is an ace, prepend it as an additional card with rank -1
	// But exclude it afterwards, its sole purpose is to make finding a straight easier
	highCard := cards[len(cards)-1]

	if highCard.rank == HighestRank {
		cards = append([]Card{{-1, highCard.suit}}, cards...)
	}

	rank.count = map[int]int{}
	consecutiveRankCount := 1
	suit := cards[0].suit

	for i, c := range cards {

		rank.count[c.rank]++
		suit &= c.suit

		if i > 0 && c.rank-1 == cards[i-1].rank {
			consecutiveRankCount++
		}
	}

	var isFlush, isStraight bool

	if suit == cards[0].suit {
		isFlush = true
	}

	if consecutiveRankCount == 5 {
		isStraight = true
	}

	if len(cards) == 6 { // Remove prepended ace
		cards = cards[1:]
		delete(rank.count, -1)
	}

	var pairs int
	var isThreeOfAKind, isFourOfAKind bool

	for _, count := range rank.count {

		switch count {
		case 2:
			pairs++

		case 3:
			isThreeOfAKind = true

		case 4:
			isFourOfAKind = true
		}
	}

	switch {

	case isStraight && isFlush:
		rank.cat = StraightFlush
	case isFourOfAKind:
		rank.cat = FourOfAKind
	case isThreeOfAKind && pairs == 1:
		rank.cat = FullHouse
	case isFlush:
		rank.cat = Flush
	case isStraight:
		rank.cat = Straight
	case isThreeOfAKind:
		rank.cat = ThreeOfAKind
	case pairs == 2:
		rank.cat = TwoPairs
	case pairs == 1:
		rank.cat = OnePair
	default:
		rank.cat = HighCard
	}

	return
}

func evaluateTie(h1, h2 Hand) (result int) {

	switch h1.rank.cat {

	case HighCard, Flush:
		result = compareHighCard(h1, h2)
	case OnePair, TwoPairs, ThreeOfAKind, FullHouse, FourOfAKind:
		result = compareRankCount(h1, h2)
	case Straight, StraightFlush:
		result = compareStraight(h1, h2)
	}

	return
}

func compareHighCard(h1, h2 Hand) int {

	for i := len(h1.cards) - 1; i >= 0; i-- {

		rank1, rank2 := h1.cards[i].rank, h2.cards[i].rank

		switch {
		case rank1 == rank2:
			continue
		case rank1 < rank2:
			return -1
		case rank1 > rank2:
			return 1
		}

	}

	return 0

}

func compareRankCount(h1, h2 Hand) int {

	var rc1 [][2]int

	for r, c := range h1.rank.count {
		rc1 = append(rc1, [2]int{r, c})
	}

	sort.SliceStable(rc1, func(i, j int) bool { return rc1[i][1] < rc1[j][1] })

	var rc2 [][2]int

	for r, c := range h2.rank.count {
		rc2 = append(rc2, [2]int{r, c})
	}

	sort.SliceStable(rc2, func(i, j int) bool { return rc2[i][1] < rc2[j][1] })

	for i := len(rc1) - 1; i >= 0; i-- {

		if rc1[i][1] != rc2[i][1] {
			panic("compareRankCount should only be called when evaluating a tie")
		}

		rank1, rank2 := rc1[i][0], rc2[i][0]

		switch {

		case rank1 == rank2:
			continue

		case rank1 < rank2:
			return -1

		case rank1 > rank2:
			return 1
		}

	}

	return 0

}

func compareStraight(h1, h2 Hand) int {

	h1HighCard, h2HighCard := h1.cards[len(h1.cards)-1], h2.cards[len(h2.cards)-1]

	// Check if the hand is a 5-high straight starting with an ace
	if h1HighCard.rank == HighestRank && h1.cards[0].rank == 0 {
		h1HighCard = h1.cards[len(h1.cards)-2]
	}

	if h2HighCard.rank == HighestRank && h2.cards[0].rank == 0 {
		h2HighCard = h2.cards[len(h2.cards)-2]
	}

	switch {
	case h1HighCard.rank < h2HighCard.rank:
		return -1
	case h1HighCard.rank > h2HighCard.rank:
		return 1
	default:
		return 0
	}
}
