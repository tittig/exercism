package strain

type Ints []int
type Lists [][]int
type Strings []string

//keep function for Ints
func (list *Ints) Keep(f func(int) bool) Ints {
	var output Ints

	for _, el := range *list {
		if f(el) {
			output = append(output, el)
		}

	}
	return output
}

//discard function for Ints
func (list *Ints) Discard(f func(int) bool) Ints {
	var output Ints

	for _, el := range *list {
		if !f(el) {
			output = append(output, el)
		}

	}
	return output
}

func (list *Lists) Keep(f func([]int) bool) Lists {
	var output Lists

	for _, el := range *list {
		if f(el) {
			output = append(output, el)
		}

	}

	return output
}

func (list *Strings) Keep(f func(string) bool) Strings {
	var output Strings

	for _, el := range *list {
		if f(el) {
			output = append(output, el)
		}

	}
	return output
}
