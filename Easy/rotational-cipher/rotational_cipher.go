package rotationalcipher

import (
	"strings"
	"unicode"
)

func RotationalCipher(inputPlain string, inputShiftKey int) string {
	alpha := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
		"e": 5,
		"f": 6,
		"g": 7,
		"h": 8,
		"i": 9,
		"j": 10,
		"k": 11,
		"l": 12,
		"m": 13,
		"n": 14,
		"o": 15,
		"p": 16,
		"q": 17,
		"r": 18,
		"s": 19,
		"t": 20,
		"u": 21,
		"v": 22,
		"w": 23,
		"x": 24,
		"y": 25,
		"z": 26,
	}
	alphaReversed := map[int]string{
		1:  "a",
		2:  "b",
		3:  "c",
		4:  "d",
		5:  "e",
		6:  "f",
		7:  "g",
		8:  "h",
		9:  "i",
		10: "j",
		11: "k",
		12: "l",
		13: "m",
		14: "n",
		15: "o",
		16: "p",
		17: "q",
		18: "r",
		19: "s",
		20: "t",
		21: "u",
		22: "v",
		23: "w",
		24: "x",
		25: "y",
		26: "z",
	}
	var output string = ""

	if inputShiftKey == 0 || inputShiftKey == 26 {
		return inputPlain
	} else {
		for _, el := range inputPlain {
			//check if char is a letter
			if !unicode.IsLetter(el) {
				output += string(el)
			} else {
				output += rotateAlpha(strings.ToLower(string(el)), inputShiftKey, alpha, alphaReversed, unicode.IsUpper(el))
			}
		}
	}

	return output
}

//function to apply  inputShiftKey rotation on char s
func rotateAlpha(s string, inputShiftKey int, alpha map[string]int, alphaReversed map[int]string, b bool) string {
	if !b {
		switch {
		case inputShiftKey > alpha[s] && inputShiftKey+alpha[s] > 26:
			return alphaReversed[alpha[s]+inputShiftKey-26]
		case alpha[s] > inputShiftKey && inputShiftKey+alpha[s] > 26:
			return alphaReversed[alpha[s]-(26-inputShiftKey)]
		default:
			return alphaReversed[alpha[s]+inputShiftKey]
		}
	} else {
		switch {
		case inputShiftKey > alpha[s] && inputShiftKey+alpha[s] > 26:
			return strings.ToUpper(alphaReversed[alpha[s]+inputShiftKey-26])
		case alpha[s] > inputShiftKey && inputShiftKey+alpha[s] > 26:
			return strings.ToUpper(alphaReversed[alpha[s]-(26-inputShiftKey)])
		default:
			return strings.ToUpper(alphaReversed[alpha[s]+inputShiftKey])
		}
	}
}
