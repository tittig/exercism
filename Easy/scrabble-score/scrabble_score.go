package scrabble

import "strings"

func Score(word string) int {
	//create map for letter score
	point := make(map[string]int)
	point["a"] = 1
	point["b"] = 3
	point["c"] = 3
	point["d"] = 2
	point["e"] = 1
	point["f"] = 4
	point["g"] = 2
	point["h"] = 4
	point["i"] = 1
	point["j"] = 8
	point["k"] = 5
	point["l"] = 1
	point["m"] = 3
	point["n"] = 1
	point["o"] = 1
	point["p"] = 3
	point["q"] = 10
	point["r"] = 1
	point["s"] = 1
	point["t"] = 1
	point["u"] = 1
	point["v"] = 4
	point["w"] = 4
	point["x"] = 8
	point["y"] = 4
	point["z"] = 10

	var score int = 0

	for _, el := range strings.ToLower(word) {
		score += point[string(el)]
	}

	return score

}
