package booking

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func Split(r rune) bool {
	return r == ':' || r == ' ' || r == '/' || r == ','
}

// Schedule returns a time.Time from a string containing a date
func Schedule(date string) time.Time {
	d, err := time.Parse("1/2/2006 15:04:05", date)
	if err != nil {
		panic(err)
	}

	return d
}

// HasPassed returns whether a date has passed
func HasPassed(date string) bool {

	splitDate := strings.FieldsFunc(date, Split)

	year, _ := strconv.Atoi(splitDate[2])
	month, _ := strconv.Atoi(splitDate[0])
	day, _ := strconv.Atoi(splitDate[1])
	hour, _ := strconv.Atoi(splitDate[3])
	min, _ := strconv.Atoi(splitDate[4])
	sec, _ := strconv.Atoi(splitDate[5])

	newdate := time.Date(year, time.Month(month), day, hour, min, sec, 0, time.UTC)
	fmt.Println(newdate)

	if newdate.Before(time.Now()) {
		return true
	} else {
		return false
	}
}

// IsAfternoonAppointment returns whether a time is in the afternoon
func IsAfternoonAppointment(date string) bool {

	splitDate := strings.FieldsFunc(date, Split)

	hour, _ := strconv.Atoi(splitDate[4])

	switch {
	case hour >= 12 && hour < 18:
		return true
	default:
		return false
	}
}

// Description returns a formatted string of the appointment time
func Description(date string) string {

	d, err := time.Parse("1/2/2006 15:04:05", date)
	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("You have an appointment on %s", d.Format("Monday, January 2, 2006, at 15:04."))
}

// AnniversaryDate returns a Time with this year's anniversary
func AnniversaryDate() time.Time {
	t, _ := time.Parse("2006-01-2", fmt.Sprintf("%d-09-15", time.Now().Year()))

	return t
}
