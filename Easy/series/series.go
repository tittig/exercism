package series

// All returns a list of all substrings of s with length n.
func All(n int, s string) []string {
	var output []string

	for i, _ := range s {
		if i+n > len(s) {
			break
		}

		output = append(output, s[i:i+n])
	}

	return output
}

// UnsafeFirst returns the first substring of s with length n.
func UnsafeFirst(n int, s string) string {
	res, ok := First(n, s)

	if ok {
		return res
	} else {
		panic(nil)
	}
}

func First(n int, s string) (first string, ok bool) {
	if n > len(s) {
		return s, false
	}

	return s[0:n], true

}
