// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package triangle should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package triangle

import "math"

// Notice KindFromSides() returns this type. Pick a suitable data type.
type Kind string

const (
	NaT = "not a triangle" // not a triangle
	Equ = "equilateral"    // equilateral
	Iso = "isosceles"      // isosceles
	Sca = "scalene"        // scalene
)

// KindFromSides should have a comment documenting it.
func KindFromSides(a, b, c float64) Kind {
	var k Kind

	if a > b+c || b > a+c || c > a+b || a <= 0 || b <= 0 || c <= 0 || math.IsNaN(a) || math.IsNaN(b) || math.IsNaN(c) || math.IsInf(a, 0) || math.IsInf(b, 0) || math.IsInf(c, 0) {
		return NaT
	} else {

		switch {
		case a == b && a == c:
			return Equ
		case a == b || a == c || b == c:
			return Iso
		case a != b && a != c && b != c:
			return Sca
		}
	}

	return k
}
