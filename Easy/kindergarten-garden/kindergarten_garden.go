package kindergarten

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

type Garden map[string][]string

var PlantsMap = map[rune]string{
	'R': "radishes",
	'C': "clover",
	'G': "grass",
	'V': "violets",
}

func NewGarden(diagram string, children []string) (*Garden, error) {
	var g Garden = map[string][]string{}

	//split by line and remove the empty firts line
	lines := strings.Split(diagram, "\n")
	lines = lines[1:]

	//remove dupplicate name
	children = RemoveDuplicateStr(children)

	//order childen by alphabetic name
	orderedChildren := make([]string, len(children))
	copy(orderedChildren, children)
	sort.Strings(sort.StringSlice(orderedChildren))

	//check children input
	if orderedChildren == nil {
		return nil, errors.New("no children")
	}
	//check diagram input
	if len(lines) != 2 {
		return nil, fmt.Errorf("expected 2 lines in diagram bug got %v", len(lines))
	}
	for _, l := range lines {
		if len(l) != 2*len(orderedChildren) {
			return nil, errors.New("number of plants in row is illegal")
		}
	}

	//associate children with plant
	plant_count := 0
	child_num := 0

	for _, l := range lines {
		for _, plant := range l {
			if plant_count != 0 && plant_count%2 == 0 {
				child_num++
			}

			//check plant validity
			s, ok := PlantsMap[plant]
			if !ok {
				return nil, errors.New("one or more plants have invalid codes")
			}

			//append in garden
			g[orderedChildren[child_num]] = append(g[orderedChildren[child_num]], s)
			plant_count++
		}

		child_num = 0
		plant_count = 0
	}

	return &g, nil
}

//Plants enable to get plant list for one children
func (g *Garden) Plants(child string) (plants []string, ok bool) {
	plants, ok = (*g)[child]

	return plants, ok
}

//RemoveDuplicateStr remove duplicate value in slice
func RemoveDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
