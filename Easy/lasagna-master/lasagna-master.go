package lasagna

// TODO: define the 'PreparationTime()' function
func PreparationTime(layers []string, avgTime int) int {

	if avgTime == 0 {
		return len(layers) * 2
	} else {
		return len(layers) * avgTime
	}

}

// TODO: define the 'Quantities()' function
func Quantities(layers []string) (nb_noodles int, nb_sauce float64) {

	for _, el := range layers {
		if el == "noodles" {
			nb_noodles += 50
		}
		if el == "sauce" {
			nb_sauce += 0.2
		}
	}

	return nb_noodles, nb_sauce
}

// TODO: define the 'AddSecretIngredient()' function
func AddSecretIngredient(friendsList, myList []string) []string {

	return append(myList, friendsList[len(friendsList)-1])
}

// TODO: define the 'ScaleRecipe()' function
func ScaleRecipe(quantity []float64, nb int) []float64 {
	out := make([]float64, len(quantity))

	for i, v := range quantity {
		out[i] = (v / 2) * float64(nb)
	}

	return out
}
