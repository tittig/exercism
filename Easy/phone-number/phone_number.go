package phonenumber

import (
	"errors"
	"fmt"
	"unicode"
)

func Number(phone string) (string, error) {

	var output string = ""

	for i, el := range phone {
		if unicode.IsNumber(el) {
			output += string(el)
		}

		i++
	}

	//check len of number phone
	switch len(output) {
	case 10:
		if output[0] < '2' || output[0] > '9' || output[3] < '2' || output[3] > '9' {
			return "", errors.New("first digit isn't between 2 and 9")
		}
		return output, nil

	case 11:
		if output[0] != '1' {
			return "", errors.New("phone number has 11 digits and doesn't start by 1")
		} else {
			if output[1] < '2' || output[1] > '9' || output[4] < '2' || output[4] > '9' {
				return "", errors.New("first digit isn't between 2 and 9")
			} else {
				return output[1:], nil
			}
		}

	default:
		return "", errors.New("phone number has less 10 digits")
	}
}

func AreaCode(phone string) (string, error) {

	phone, err := Number(phone)

	if err != nil {
		return "", err
	}

	return phone[0:3], err
}

func Format(phone string) (string, error) {

	phone, err := Number(phone)

	if err != nil {
		return "", err
	}

	return fmt.Sprintf("(%s) %s-%s", phone[0:3], phone[3:6], phone[6:]), nil
}
