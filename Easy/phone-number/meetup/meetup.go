package meetup

import (
	"time"
)

type WeekSchedule int

const (
	First  WeekSchedule = 1
	Second WeekSchedule = 2
	Third  WeekSchedule = 3
	Fourth WeekSchedule = 4
	Last   WeekSchedule = 5
	Teenth WeekSchedule = 6
)

//teenth entre 13 et 19
func Day(weekschedule WeekSchedule, weekday time.Weekday, month time.Month, year int) int {

	switch weekschedule {
	case 1:
		for i := 1; i < 8; i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	case 2:
		for i := 8; i < 15; i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	case 3:
		for i := 15; i < 22; i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	case 4:
		for i := 22; i < 29; i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	case 5:
		firstOfMonth := time.Date(year, month, 1, 0, 0, 0, 0, time.UTC)
		lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
		for i := lastOfMonth.Day() - 6; i <= lastOfMonth.Day(); i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	case 6:
		for i := 13; i < 20; i++ {
			t := time.Date(year, month, i, 0, 0, 0, 0, time.UTC)
			if t.Weekday() == weekday {
				return t.Day()
			}
		}
	}

	return 0
}
