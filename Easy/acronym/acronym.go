// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import "strings"

const alpha = "abcdefghijklmnopqrstuvwxyz"

func alphaOnly(s string) bool {
	for _, char := range s {
		if !strings.Contains(alpha, strings.ToLower(string(char))) {
			return false
		}
	}
	return true
}

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	var res string
	var isletter bool = false

	for _, char := range s {
		if alphaOnly(string(char)) && !isletter {
			res = res + strings.ToUpper(string(char))
			isletter = true
		} else if !alphaOnly(string(char)) && string(char) != "'" {
			isletter = false
		}
	}

	return res
}
