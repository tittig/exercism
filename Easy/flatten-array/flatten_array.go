package flatten

func Flatten(input interface{}) []interface{} {
	res := []interface{}{}
	listSlice, _ := input.([]interface{})

	for _, el := range listSlice {
		switch el.(type) {
		case int:
			res = append(res, el)
		case interface{}:
			res = append(res, Flatten(el)...)
		}
	}

	return res
}
