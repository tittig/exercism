package romannumerals

import (
	"errors"
	"sort"
)

type Toroman map[int]string

var toroman = Toroman{
	1000: "M",
	900:  "CM",
	500:  "D",
	400:  "CD",
	100:  "C",
	90:   "XC",
	50:   "L",
	40:   "XL",
	10:   "X",
	9:    "IX",
	5:    "V",
	4:    "IV",
	3:    "III",
	2:    "II",
	1:    "I",
}

func ToRomanNumeral(nb int) (result string, err error) {
	//check input
	if nb > 3000 || nb <= 0 {
		return "", errors.New("input error")
	}

	//sort map by key
	keys := make([]int, 0, len(toroman))
	for k := range toroman {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[j] < keys[i]
	})

	//algorithm
	for _, key := range keys {
		for nb >= key {
			result += toroman[key]
			nb -= key
		}
	}

	return result, nil
}
