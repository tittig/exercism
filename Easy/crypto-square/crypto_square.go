package cryptosquare

import (
	"math"

	"regexp"

	"strings"
)

// Encode transforms a string input to a crypto square output

func Encode(input string) string {

	reg, _ := regexp.Compile("[^[:alnum:]]+")

	processedString := strings.ToLower(reg.ReplaceAllString(input, ""))

	length := len(processedString)

	colCount := int(math.Ceil(math.Sqrt(float64(len(processedString)))))

	rowCount := int(math.Ceil(float64(length) / float64(colCount)))

	matrix := make([][]string, colCount)

	for rowIndex := 0; rowIndex < colCount; rowIndex++ {

		matrix[rowIndex] = make([]string, rowCount)

	}

	for colIndex := 0; colIndex < rowCount; colIndex++ {

		upperBound := (colIndex + 1) * colCount

		if upperBound > len(processedString) {

			upperBound = len(processedString)

		}

		substr := processedString[colIndex*colCount : upperBound]

		if len(substr) < colCount {

			substr = rightPad(substr, colCount)

		}

		for rowIndex := 0; rowIndex < colCount; rowIndex++ {

			matrix[rowIndex][colIndex] = string(substr[rowIndex])

		}

	}

	return flatten(matrix)

}

func rightPad(str string, length int) string {

	for {

		str += " "

		if len(str) == length {

			return str

		}

	}

}

func flatten(m [][]string) string {

	rows := make([]string, len(m))

	for i, row := range m {

		rows[i] = strings.Join(row[:len(row)], "")

	}

	return strings.Join(rows, " ")

}
