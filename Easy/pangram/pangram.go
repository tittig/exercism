package pangram

import (
	"strings"
	"unicode"
)

func IsPangram(input string) bool {
	letter_map := make(map[rune]int)

	//change input in lowercase and count different letter
	for _, letter := range strings.ToLower(input) {
		if unicode.IsLetter(letter) {
			letter_map[letter] += 1
		}
	}

	//check if there are 26 letter
	if len(letter_map) == 26 {
		return true
	} else {
		return false
	}
}
