package letter

type FreqMap map[rune]int

func Frequency(s string) FreqMap {
	m := FreqMap{}
 	for _, r := range s {
		m[r]++
	}
	return m
}

func myFrequency(s string,c chan FreqMap ) {
	m := FreqMap{}
 	for _, r := range s {
		m[r]++
	}
	c <- m
}

func ConcurrentFrequency(list []string) FreqMap {
	c := make(chan FreqMap, len(list))

 	for _, s := range list {
		go myFrequency(s, c)
	}      

	res := make(FreqMap)

	for range list {   
		for letter, freq := range <-c {
			res[letter] += freq       
		}
	}

	return res
}