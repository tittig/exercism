package sieve

func Sieve(limit int) (out []int) {
	//list = [2,..,l]
	l := make([]bool, limit+1)
	if limit <= 0 {
		return out
	}

	//init l
	for i := 0; i <= limit; i++ {
		l[i] = true
	}
	l[0] = false
	l[1] = false

	//change to false all no prime number
	i := 2
	for {
		if i*i > limit {
			break
		}

		if l[i] {
			for j := i * i; j <= limit; j = j + i {
				l[j] = false

			}
		}
		i++
	}

	//create the out : get all true value in l
	for i, el := range l {
		if el {
			out = append(out, i)
		}
	}

	return out
}
