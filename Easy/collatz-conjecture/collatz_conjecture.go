package collatzconjecture

import "fmt"

// CollatzConjecture computes the number of items in the collatz sequence for a given `n`
func CollatzConjecture(n int) (int, error) {
	// Collatz Sequence is only defined for n >= 1
	if n < 1 {
		return 0, fmt.Errorf("Collatz sequence not defined for number: %d", n)
	}

	seq := make(chan int)

	go collatzGenerator(n, seq)
	return length(seq) - 1, nil
}

// Helper function to take the length of the channel (ie. the length of the collatz sequence)
func length(seq <-chan int) int {
	steps := 0

	for range seq {
		steps++
	}

	return steps
}

// collatzGenerator is a helper function to produce all the values in the collatz sequence
func collatzGenerator(n int, seq chan<- int) {
	seq <- n

	for n != 1 {
		if n%2 == 0 {
			n /= 2
		} else {
			n = 3*n + 1
		}
		seq <- n
	}

	close(seq)
}
