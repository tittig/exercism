package annalyn

// CanFastAttack can be executed only when the knight is sleeping
func CanFastAttack(knightIsAwake bool) bool {
    return  !knightIsAwake
}

// CanSpy can be executed if at least one of the characters is awake
func CanSpy(knightIsAwake, archerIsAwake, prisonerIsAwake bool) bool {
    return knightIsAwake || archerIsAwake || prisonerIsAwake
}

// CanSignalPrisoner can be executed if prisoner is awake and the archer is sleeping
func CanSignalPrisoner(archerIsAwake, prisonerIsAwake bool) bool {
    if archerIsAwake==prisonerIsAwake {
        return false
    } else {
    return prisonerIsAwake
    }
}

// CanFreePrisoner can be executed if prisoner is awake and the other 3 characters are asleep
// or if Annalyn's pet dog is with her and the archer is sleeping
func CanFreePrisoner(knightIsAwake, archerIsAwake, prisonerIsAwake, dogIsPresent bool) bool {
	if knightIsAwake==false && archerIsAwake==false && (prisonerIsAwake==true || dogIsPresent==true) {
        return true
    }
	if knightIsAwake==true && archerIsAwake==false && prisonerIsAwake==true && dogIsPresent==true {
        return true
    }
	 return false
}
