package clock

import "fmt"

type Clock struct {
	Hour   int
	Minute int
}

func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.Hour, c.Minute)
}

func New(h, m int) Clock {
	min := m % 60
	hours := (h + int(m/60)) % 24

	if hours < 0 {
		hours += 24
	}
	if min < 0 {
		hours -= 1
		min += 60
	}
	return Clock{hours, min}
}

func (c Clock) Add(m int) Clock {
	c.Hour = (c.Hour + int((c.Minute+m)/60)) % 24
	c.Minute = (c.Minute + m) % 60

	return c
}

func (c Clock) Subtract(m int) Clock {
	c.Hour = (c.Hour + int((c.Minute-m)/60)) % 24
	c.Minute = (c.Minute - m) % 60

	if c.Minute < 0 {
		c.Minute += 60
		c.Hour = c.Hour - 1
	}
	if c.Hour < 0 {
		c.Hour = c.Hour + 24
	}

	return c
}
