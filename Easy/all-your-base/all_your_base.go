package allyourbase

import "errors"

// ConvertToBase converts a number, represented as a sequence of digits in one base, to any other base

func ConvertToBase(base int, digits []int, outputBase int) (conversion []int, err error) {

	if base < 2 {
		return []int{0}, errors.New("input base must be >= 2")
	}

	if outputBase < 2 {
		return []int{0}, errors.New("output base must be >= 2")
	}

	var sum int

	for _, d := range digits {
		if d >= base || d < 0 {
			return nil, errors.New("all digits must satisfy 0 <= d < input base")
		}

		sum = sum*base + d
	}

	if sum == 0 {
		return []int{0}, nil
	}

	for ; sum > 0; sum /= outputBase {
		conversion = append([]int{sum % outputBase}, conversion...)
	}

	return conversion, nil
}
