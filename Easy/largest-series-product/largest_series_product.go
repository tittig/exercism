package lsproduct

import (
	"errors"
	"fmt"
	"strconv"
	"unicode"
)

//find the largest product
func LargestSeriesProduct(digits string, span int) (int64, error) {
	var largestProduct int64 = 0
	check, err := CheckDigits(digits, span)

	fmt.Println(digits)

	if !check {
		return largestProduct, err
	} else {
		for i := 0; i <= len(digits)-span; i++ {
			product := ComputeProduct(digits[i : i+span])

			if largestProduct < product {
				largestProduct = product
			}
		}
	}

	return largestProduct, err
}

//compute the product of span size
func ComputeProduct(digits string) int64 {
	var product int64 = 1
	for _, el := range digits {
		var conv, _ = strconv.Atoi((string(el)))
		product = product * int64(conv)
	}

	return product
}

//check digits validity
func CheckDigits(digits string, span int) (bool, error) {
	//check span
	if span < 0 {
		return false, errors.New("span is negatif")
	}

	//check digits size in function of span
	if len(digits) < span {
		return false, errors.New("span is bigest than len(digits)")
	}

	//check if digits contains only digit
	if !isInt(digits) {
		return false, errors.New("digits doesn't contains only digit")
	}

	return true, nil
}

//check if a string contains only digit
func isInt(s string) bool {
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}
