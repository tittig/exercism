package cars

// CalculateProductionRatePerHour for the assembly line, taking into account
func CalculateProductionRatePerHour(speed int) float64 {
	return float64(speed*221) * successRate(speed)
}

// CalculateProductionRatePerMinute describes how many working items are
func CalculateProductionRatePerMinute(speed int) int {
	return (int(CalculateProductionRatePerHour(speed) / 60))
}

// successRate is used to calculate the ratio of an item being created without
func successRate(speed int) float64 {
	if speed == 0 {
		return 0.0
	}

	if speed < 5 {
		return 1.0
	}

	if speed >= 9 {
		return 0.77
	}
	return 0.9
}
