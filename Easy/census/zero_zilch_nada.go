package zero

// EmptyBool returns an empty (zero value) bool
func EmptyBool() bool {
	var empty bool
	return empty
}

// EmptyInt returns an empty (zero value) int
func EmptyInt() int {
	var empty int
	return empty
}

// EmptyString returns an empty (zero value) string
func EmptyString() string {
	var empty string
	return empty
}

// EmptyFunc returns an empty (zero value) func
func EmptyFunc() func() {
	var empty func() 
	return empty
}

// EmptyPointer returns an empty (zero value) pointer
func EmptyPointer() *int {
	var empty *int
	return empty
}

// EmptyMap returns an empty (zero value) map
func EmptyMap() map[int]int {
	var empty map[int]int
	return empty
}

// EmptySlice returns an empty (zero value) slice
func EmptySlice() []int {
	var empty []int
	return empty
}

// EmptyChannel returns an empty (zero value) channel
func EmptyChannel() chan int {
	var empty chan int
	return empty
}

// EmptyInterface returns an empty (zero value) interface
func EmptyInterface() interface{} {
	var empty interface{}
	return empty
}
