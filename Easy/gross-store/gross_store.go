package gross

// Units store the Gross Store unit measurement
func Units() map[string]int {
	return map[string]int{
		"quarter_of_a_dozen": 3,
		"half_of_a_dozen":    6,
		"dozen":              12,
		"small_gross":        120,
		"gross":              144,
		"great_gross":        1728,
	}
}

// NewBill create a new bill
func NewBill() map[string]int {
	return map[string]int{}
}

// AddItem add item to customer bill
func AddItem(bill, units map[string]int, item, unit string) bool {
	value, exists := units[unit]
	if exists {
		bill[item] = value
	}

	return exists
}

// RemoveItem remove item from customer bill
func RemoveItem(bill, units map[string]int, item, unit string) bool {

	_, foundItem := bill[item]
	_, foundUnit := units[unit]

	if !foundItem || !foundUnit {
		return false
	}

	newQulity := bill[item] - units[unit]

	if newQulity < 0 {
		return false
	} else if newQulity == 0 {
		delete(bill, item)

		return true
	} else {

		bill[item] = newQulity
		return true
	}
}

// GetItem return the quantity of item that the customer has in his/her bill
func GetItem(bill map[string]int, item string) (int, bool) {
	if bill[item] == 0 {
		return 0, false
	} else {
		return bill[item], true
	}
}
