package wordcount

import (
	"strings"
)

type Frequency map[string]int

func WordCount(input string) Frequency {
	freq := make(Frequency)

	//normalize and clean input
	normalize_input := Normalize(input)
	clean_input := strings.TrimFunc(normalize_input, CustomTrim)
	//split input after clean and normalize operation
	split_input := strings.FieldsFunc(clean_input, CustomSplit)

	//build frequency
	for _, word := range split_input {
		//remove single quote
		first := word[0]
		last := word[len(word)-1]
		if first == '\'' && last == '\'' {
			word = word[1 : len(word)-1]
		}

		freq[word] += 1
	}

	return freq
}

func CustomSplit(r rune) bool {
	return r == ':' || r == ',' || r == ' ' || r == '\n' || r == '.'
}

func CustomTrim(r rune) bool {
	return r == '!' || r == '&' || r == '@' || r == '$' || r == '%' || r == '^'
}

func Normalize(s string) string {
	s = strings.ToLower(s)
	return s
}
