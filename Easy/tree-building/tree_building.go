package tree

import (
	"errors"
	"sort"
)

type Record struct {
	ID     int
	Parent int
}

type Node struct {
	ID       int
	Children []*Node
}

//main function
func Build(records []Record) (*Node, error) {
	if len(records) < 1 {
		return nil, nil
	} else {
		//sort input to have a output sorted
		sort.Slice(records, func(i, j int) bool { return records[i].ID < records[j].ID })
		//check the input data to avoid issue
		err := checkInput(records)
		if err != nil {
			return nil, err
		} else {
			//init the recursivity
			var root_node Node = *init_search_children(0, records)

			//recursivity on Children of the root_node
			recursive_search_children(root_node, records)

			//check output
			err := checkOutput(root_node, records)

			return &root_node, err
		}
	}
}

//function to find children in starting by the node 0
func init_search_children(id int, records []Record) *Node {
	var root_node Node
	root_node.ID = id

	for _, record := range records {
		if record.Parent == id && record.ID != id {
			var children_node Node
			children_node.ID = record.ID
			root_node.Children = append(root_node.Children, &children_node)
		}
	}
	return &root_node
}

//function to make recursivity with the firts node 0
func recursive_search_children(node Node, records []Record) {
	for _, child := range node.Children {
		for _, record := range records {
			if record.Parent == child.ID && record.ID != child.ID {
				var children_node Node
				children_node.ID = record.ID
				child.Children = append(child.Children, &children_node)
			}
		}

		recursive_search_children(*child, records)
	}
}

//function to check input data
func checkInput(records []Record) error {
	sort.Slice(records, func(i, j int) bool { return records[i].ID < records[j].ID })

	//root node has parent
	if records[0].Parent != 0 {
		return errors.New("root node has parent")
	}
	//no root node
	if records[0].ID != 0 {
		return errors.New("no root node")
	}
	//duplicate node or duplicate root
	duplicate_frequency := make(map[Record]int)
	for _, record := range records {
		_, exist := duplicate_frequency[record]
		if exist {
			duplicate_frequency[record] += 1
			return errors.New("duplicate node")
		} else {
			duplicate_frequency[record] = 1
		}
	}

	//non-continuous
	list_node_id := make(map[int]int)
	var id_max int = -1
	for _, record := range records {
		list_node_id[record.ID] += 1
		if record.ID > id_max {
			id_max = record.ID
		}
	}
	if len(list_node_id) != id_max+1 {
		return errors.New("non-continuous")
	}

	//higher id parent of lower id
	for _, record := range records {
		if record.ID < record.Parent {
			return errors.New("higher id parent of lower id")
		}
	}

	return nil
}

//function to check output data
func checkOutput(node Node, records []Record) error {
	//cycle directly and cycle indirectly
	if len(records) != node.Len() {
		return errors.New("cycle directly or cycle indirectly ")

	} else {
		return nil
	}
}

//count the number of nodes from the node n
func (n Node) Len() int {
	var count int = 1
	for _, el := range n.Children {
		if el.Children != nil {
			count += el.Len()
		} else {
			count += 1
		}
	}
	return count
}
