package listops

// IntList represents a slice of ints.
type IntList []int

// binFunc is a type of function that takes two ints and returns the result of some computation on them.
type binFunc func(int, int) int

// predFunc is a type of function that takes an int and tells whether that int satisfies a condition or not.
type predFunc func(n int) bool

// unaryFunc is a type of function that takes an int and returns the result of some computation on it.
type unaryFunc func(x int) int

// Foldl returns the result of applying function f to an accumulator (initial value) and each element in l from left to right.
func (l IntList) Foldl(f binFunc, acc int) int {
	for _, v := range l {
		acc = f(acc, v)
	}

	return acc
}

// Foldr returns the result of applying function f each element in l and to an accumulator (initial value) from right to left.
func (l IntList) Foldr(f binFunc, acc int) int {
	for i := len(l) - 1; i >= 0; i-- {
		acc = f(l[i], acc)
	}

	return acc
}

// Filter returns an IntList with just the values in l that satisfies predicate f.
func (l IntList) Filter(f predFunc) IntList {
	rl := []int{}

	for _, v := range l {
		if f(v) {
			rl = append(rl, v)
		}
	}

	return rl
}

// Length returns the number of elements in l.
func (l IntList) Length() int {
	var cont int

	for cont = range l {
		cont++
	}

	return cont
}

// Map returns an IntList where each element is the result of applying f to the original element in l.
func (l IntList) Map(f unaryFunc) IntList {
	rl := make(IntList, len(l))

	for i, v := range l {
		rl[i] = f(v)
	}

	return rl
}

// Reverse returns a reversed copy of l
func (l IntList) Reverse() IntList {
	rl := make(IntList, len(l))

	last := len(l) - 1
	for i := last; i >= 0; i-- {
		rl[last-i] = l[i]
	}

	return rl
}

// Append returns a new IntList with the values in l2 appended to the values in l.
func (l IntList) Append(l2 IntList) IntList {
	return append(l, l2...)
}

// Concat returns a new IntList with the values in every list in lol appended to the values in l.
func (l IntList) Concat(lol []IntList) IntList {
	for _, v := range lol {
		l = l.Append(v)
	}

	return l
}
