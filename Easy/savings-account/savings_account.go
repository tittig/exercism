package savings

// FixedInterestRate has a value of 5% (5/100)

// GetFixedInterestRate returns the FixedInterestRate constant
func GetFixedInterestRate() float32 {
	const FixedInterestRate float32 = 0.05
	return FixedInterestRate
}

// DaysPerYear has a value of 365

// GetDaysPerYear returns the DaysPerYear constant
func GetDaysPerYear() int {
	const DaysPerYear int = 365
	return DaysPerYear
}

// Jan-Dec have values of 1-12
const (
	Jan int = 1
	Feb int = 2
	Mar int = 3
	Apr int = 4
	May int = 5
	Jun int = 6
	Jul int = 7
	Aug int = 8
	Sep int = 9
	Oct int = 10
	Nov int = 11
	Dec int = 12
)

// GetMonth returns the value for the given month
func GetMonth(month int) int {
	return month
}

// AccNo type for a string - this is a stub type included to demonstrate how the untyped string constant can be used where this type is required
type AccNo string

// AccountNo has a value of XF348IJ

// GetAccountNumber returns the AccountNo constant
func GetAccountNumber() AccNo {
	const AccountNo = "XF348IJ"
    return AccountNo
}
