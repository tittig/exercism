package cards

// GetItem retrieves an item from a slice at given position. The second return value indicates whether
// a the given index existed in the slice or not.
func GetItem(slice []uint8, index int) (uint8, bool) {
	if slice == nil || index < 0 || index >= len(slice) {
		return 0, false
	}
	return slice[index], true
}

// SetItem writes an item to a slice at given position overwriting an existing value.
// If the index is out of range it is be appended.
func SetItem(slice []uint8, index int, value uint8) []uint8 {
	if index >= len(slice) || index < 0 || slice == nil {
		slice = append(slice, value)
	} else {
		slice[index] = value
	}

	return slice
}

// PrefilledSlice creates a slice of given length and prefills it with the given value.
func PrefilledSlice(value, length int) []int {
	slice := []int{}

	slice = nil

	for i := 0; i < length; i++ {
		slice = append(slice, value)
	}

	return slice
}

// RemoveItem removes an item from a slice by modifying the existing slice.
func RemoveItem(slice []int, index int) []int {
	if slice == nil {
		return nil
	} else if index >= len(slice) || index < 0 {
		return slice
	}

	slice1 := slice[0:index]
	slice2 := slice[index+1:]

	for _, element := range slice2 {
		slice1 = append(slice1, element)
	}

	return slice1
}
