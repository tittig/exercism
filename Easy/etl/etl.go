// Package etl transforms Scrabble score data to a shiny new format.

        
package etl
          
import "strings"

// ScrabbleScores maps from a letter to its point value.
type ScrabbleScores map[string]int

// Legacy maps from a point value to an array of letters
type Legacy map[int][]string

// Transform converts legacy score data to a new format.
func Transform(legacy Legacy) ScrabbleScores {
	shiny := ScrabbleScores{}

	for score, letters := range legacy {
		for _, letter := range letters {
			shiny.setScore(letter, score)
		}
	}

	return shiny
}

// setScore records the score for a given letter.
func (s ScrabbleScores) setScore(letter string, score int) {
	s[strings.ToLower(letter)] = score
}