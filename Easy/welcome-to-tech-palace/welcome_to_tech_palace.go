package techpalace

import (
	"strings"
)

// WelcomeMessage returns a welcome message for the customer.
func WelcomeMessage(customer string) string {
	return "Welcome to the Tech Palace, " + strings.ToUpper(customer)
}

// AddBorder adds a border to a welcome message.
func AddBorder(welcomeMsg string, numStarsPerLine int) string {

	stars := strings.Repeat("*", numStarsPerLine)

	return stars + "\n" + welcomeMsg + "\n" + stars
}

// CleanupMessage cleans up an old marketing message.
func CleanupMessage(oldMsg string) string {
	//remove * and \n
	output := strings.TrimFunc(oldMsg, CustomClean)

	//remove space
	output = strings.TrimLeft(output, " ")
	output = strings.TrimRight(output, " ")

	return output
}

func CustomClean(r rune) bool {
	return r == '*' || r == '\n'
}
