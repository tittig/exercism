package diffsquares

import "math"

func SquareOfSum(nb int) int {
	var sum int
	for i := 0; i <= nb; i++ {
		sum += i
	}
	return int(math.Pow(float64(sum), 2))
}

func SumOfSquares(nb int) int {
	var sum int
	for i := 0; i <= nb; i++ {
		sum += int(math.Pow(float64(i), 2))
	}
	return sum
}

func Difference(nb int) int {
	return SquareOfSum(nb) - SumOfSquares(nb)
}
