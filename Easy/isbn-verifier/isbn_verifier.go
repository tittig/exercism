package isbn

import (
	"strings"
	"unicode"
)

func IsValidISBN(isbn string) bool {
	//remove "-"
	isbn = strings.ReplaceAll(isbn, "-", "")

	//build slice of isbn digit
	if len(isbn) == 10 {
		isbn_slice := make([]int, len(isbn))

		for i, char := range isbn {
			//TO DO check if only digit
			if i != 9 {
				if !unicode.IsNumber(char) {
					return false
				}

				isbn_slice[i] = check_isbn(string(char))
			}
			isbn_slice[i] = check_isbn(string(char))
		}
		if isbn_slice[len(isbn)-1] == -1 {
			return false
		}

		//verification of isbn
		ver := isbn_slice[0]*10 + isbn_slice[1]*9 + isbn_slice[2]*8 + isbn_slice[3]*7 + isbn_slice[4]*6 + isbn_slice[5]*5 + isbn_slice[6]*4 + isbn_slice[7]*3 + isbn_slice[8]*2 + isbn_slice[9]*1
		if ver%11 == 0 {
			return true
		}
	}

	return false
}

func check_isbn(s string) int {
	switch s {
	case "0":
		return 0
	case "1":
		return 1
	case "2":
		return 2
	case "3":
		return 3
	case "4":
		return 4
	case "5":
		return 5
	case "6":
		return 6
	case "7":
		return 7
	case "8":
		return 8
	case "9":
		return 9
	case "X":
		return 10
	default:
		return -1
	}
}
