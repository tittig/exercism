package prime

func Nth(num int) (int, bool) {
	var count int = 0
	var nb_loop int = 2

	//manage wrong num
	if num < 1 {
		return 0, false
	}

	for {
		//count the number of Nth
		if CheckNth(nb_loop) {
			count++
		}
		//check if number of Nth is equal as num
		if count == num {
			return nb_loop, true
		}
		nb_loop++
	}
}

//check if nb is a Nth
func CheckNth(nb int) bool {
	for i := 2; i < nb; i++ {
		if nb%i == 0 {
			return false
		}
	}

	return true
}

//Next step use goroutine and chanel to speed up
