package robotname

import (
	"math/rand"
)

type Robot struct {
	name string
}

var robotsInUse = make(map[string]bool)

const letters_size, digits_size int = 2, 3

func (r *Robot) Name() (string, error) {
	if r.name == "" {
		for {
			r.name = RandomString(letters_size) + RandomInt(digits_size)
			if !robotsInUse[r.name] {
				break
			}
		}
		robotsInUse[r.name] = true
		return r.name, nil
	} else {
		return r.name, nil
	}
}

func (r *Robot) Reset() {
	r.name = ""
}

//create a random string
func RandomString(n int) string {
	var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

//create a random int
func RandomInt(n int) string {
	var letters = []rune("0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}
