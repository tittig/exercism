package cipher

import (
	"strings"
	"unicode"
)

type Mycipher []int

//Caesar
func NewCaesar() Mycipher {
	return Mycipher{3}
}

//Shift
func NewShift(nb int) Mycipher {
	if nb == 0 || nb < -25 || nb > 25 {
		return nil
	}

	return Mycipher{nb}
}

//Vigenere
func NewVigenere(input string) Mycipher {
	var out Mycipher

	//check input
	for _, el := range input {
		switch {
		case el >= 'a' && el <= 'z':
			out = append(out, int(el-'a'))
		default:
			return nil
		}

	}

	//check if doesn't contain only 0
	sum := 0
	for _, el := range out {
		sum += el
	}
	if sum == 0 {
		return nil
	}

	return out
}

//Encode and decode function
func (c Mycipher) Encode(input string) (out string) {

	//clean string
	var clean_string string
	for _, el := range input {
		if unicode.IsLetter(el) {
			clean_string += strings.ToLower(string(el))
		}
	}

	//code output
	switch {
	case len(c) == 1 && c[0] > 0:
		for _, el := range clean_string {
			out += string((el-'a'+rune(c[0]))%26 + 'a')
		}
	case len(c) == 1 && c[0] < 0:
		for _, el := range clean_string {
			out += string((el-'z'+rune(c[0]))%26 + 'z')
		}
	case len(c) > 1:
		for i, el := range clean_string {
			out += string((el-'a'+rune(c[i%len(c)]))%26 + 'a')
		}
	default:
		return ""
	}

	return out
}

func (c Mycipher) Decode(input string) (out string) {
	//clean string
	var clean_string string
	for _, el := range input {
		if unicode.IsLetter(el) {
			clean_string += strings.ToLower(string(el))
		}
	}

	//code output
	switch {
	case len(c) == 1 && c[0] > 0:
		for _, el := range clean_string {
			out += string((el-'z'-rune(c[0]))%26 + 'z')
		}
	case len(c) == 1 && c[0] < 0:
		for _, el := range clean_string {
			out += string((el-'a'-rune(c[0]))%26 + 'a')
		}
	case len(c) > 1:
		for i, el := range clean_string {
			out += string((el-'z'-rune(c[i%len(c)]))%26 + 'z')
		}
	default:
		return ""
	}

	return out
}
