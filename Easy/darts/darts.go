package darts

import (
	"math"
)

const raduis = 10

var center = Point{0, 0}

type Point struct {
	x float64
	y float64
}

func Score(x float64, y float64) int {
	switch {
	case distance(x, y) <= raduis && distance(x, y) > raduis/2:
		return 1
	case distance(x, y) <= raduis/2 && distance(x, y) > raduis/10:
		return 5
	case distance(x, y) <= raduis/10 && distance(x, y) >= 0:
		return 10
	default:
		return 0
	}
}

func distance(x float64, y float64) float64 {
	return math.Sqrt(math.Pow(x-center.x, 2) + math.Pow(y-center.y, 2))
}
