package atbash

import (
	"strings"
)

func Atbash(input string) string {
	//map to make the crypto
	alphamap := map[rune]rune{
		'a': 'z',
		'b': 'y',
		'c': 'x',
		'd': 'w',
		'e': 'v',
		'f': 'u',
		'g': 't',
		'h': 's',
		'i': 'r',
		'j': 'q',
		'k': 'p',
		'l': 'o',
		'm': 'n',
		'n': 'm',
		'o': 'l',
		'p': 'k',
		'q': 'j',
		'r': 'i',
		's': 'h',
		't': 'g',
		'u': 'f',
		'v': 'e',
		'w': 'd',
		'x': 'c',
		'y': 'b',
		'z': 'a',
	}
	var output string = ""

	//ignore Upper case
	input = strings.ToLower(input)
	//remove space
	input = strings.TrimSpace(input)
	// remove .
	input = strings.Trim(input, ".")

	//build the output
	var loop_count int = 0
	for _, el := range input {

		if loop_count == 5 {
			output += " "
			loop_count = 0
		}

		switch {
		case el >= 'a' && el <= 'z':
			output += string(alphamap[el])
			loop_count++
		case el >= '0' && el <= '9':
			output += string(el)
			loop_count++
		}

	}

	return output
}
