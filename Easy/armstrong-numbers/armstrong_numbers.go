package armstrong

import (
	"math"
)

//CountDigits count the number of digit in a number (for ex: 323 has 3 digits)
func CountDigits(i int) (count int) {
	for i != 0 {
		i /= 10
		count = count + 1
	}
	return count
}

//SumPow compute the Armstrong compute
func SumPow(input int) int {
	remainder := 0
	sumResult := 0
	count := CountDigits(input)

	for input != 0 {
		remainder = input % 10
		sumResult += int(math.Pow(float64(remainder), float64(count)))
		input = input / 10

	}

	return sumResult
}

//IsNumber checks if the input is a Armstrong number
func IsNumber(input int) bool {

	if SumPow(input) == input {
		return true
	} else {
		return false
	}

}
