//Package weather content weather by location
package weather



var (
    //CurrentCondition contents the weather
	CurrentCondition string
    //CurrentLocation contents a location
	CurrentLocation  string
)

//Log function return a log message about the weather in a location
func Log(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
