package sublist

type Relation string

func Sublist(listOne []int, listTwo []int) Relation {

	switch {
	case len(listOne) == len(listTwo) && IsSublist(listOne, listTwo):
		return "equal"

	case len(listOne) > len(listTwo) && IsSublist(listTwo, listOne):
		return "superlist"

	case len(listOne) < len(listTwo) && IsSublist(listOne, listTwo):
		return "sublist"

	default:
		return "unequal"

	}
}

//IsSubList checks is listOne is a sublist of listTwo
func IsSublist(listOne []int, listTwo []int) bool {
	var ok bool = true

	for i := 0; i+len(listOne) < len(listTwo)+1; i++ {
		ok = Equal(listOne, listTwo[i:i+len(listOne)])

		if ok {
			break
		}
	}

	return ok

}

//Equal checks is list a is equal of list b
func Equal(a, b []int) bool {

	if len(a) != len(b) {
		return false
	}

	for i, v := range a {
		if v != b[i] {
			return false
		}
	}

	return true
}
