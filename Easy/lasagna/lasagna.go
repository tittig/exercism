package lasagna

// TODO: define the 'OvenTime()' function
func OvenTime() int {
	return 40
}

// TODO: define the 'RemainingOvenTime()' function
func RemainingOvenTime(min int) int {
	return OvenTime() - min
}

// TODO: define the 'PreparationTime()' function
func PreparationTime(l int) int {
	return 2 * l
}

// TODO: define the 'ElapsedTime()' function
func ElapsedTime(l, min int) int {
	return min + PreparationTime(l)
}
