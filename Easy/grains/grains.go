package grains

import (
	"errors"
	"math"
)

const nbtotal = 64

func Total() uint64 {

	/*
		var total uint64 = 0
		for i := 1; i < 65; i++ {
			a, _ := Square(i)
			total += a
		}
	*/
	return 1<<nbtotal - 1
}

//18446744073709551615
//  math.Pow(2, 64)=9223372036854775808 pb !!!

func Square(nb int) (uint64, error) {
	if nb < 1 || nb > 64 {
		return uint64(nb), errors.New("la case n'est pas compris entre 1 et 64")
	} else {
		return uint64(math.Pow(2, float64(nb-1))), nil // or 1 << nb-1 means 1 × 2^(nb-1)
	}
}

//time 2.6s
