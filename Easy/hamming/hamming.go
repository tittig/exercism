package hamming

import "errors"

func Distance(a, b string) (int, error) {

	if len(a) == len(b) {
		var count int = 0
		for i := 0; i < len(a); i++ {
			if a[i] != b[i] {
				count++
			}
		}
		return count, nil

	} else {
		return 0, errors.New("error: coudn't compute hamming, different siz")
	}

}
