package prime

import (
	"math"
)

func Factors(nb int64) []int64 {
	var output = []int64{}
	var i int64

	for nb > 1 {
		if IsPrime(nb) {
			output = append(output, nb)
			break
		}

		for i = 2; i < nb; i++ {
			if nb%i == 0 && IsPrime(i) {
				output = append(output, i)
				nb /= i
			}
		}

	}

	return output
}

//IsPrime checks if a number is a prime number
func IsPrime(value int64) bool {
	var i int64
	for i = 2; i <= int64(math.Floor(float64(value)/2)); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}
