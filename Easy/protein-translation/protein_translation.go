package protein

import (
	"errors"
)

const (
	AUG = "Methionine"
	UUU = "Phenylalanine"
	UUC = "Phenylalanine"
	UUA = "Leucine"
	UUG = "Leucine"
	UCU = "Serine"
	UCC = "Serine"
	UCA = "Serine"
	UCG = "Serine"
	UAU = "Tyrosine"
	UAC = "Tyrosine"
	UGU = "Cysteine"
	UGC = "Cysteine"
	UGG = "Tryptophan"
	UAA = ""
	UAG = ""
	UGA = ""
)

var ErrStop = errors.New("ErrStop")
var ErrInvalidBase = errors.New("ErrInvalidBase ")

func FromCodon(input string) (string, error) {
	switch input {
	case "AUG":
		return AUG, nil
	case "UUU":
		return UUU, nil
	case "UUC":
		return UUC, nil
	case "UUA":
		return UUA, nil
	case "UUG":
		return UUG, nil
	case "UCU":
		return UCU, nil
	case "UCC":
		return UCC, nil
	case "UCA":
		return UCA, nil
	case "UCG":
		return UCG, nil
	case "UAU":
		return UAU, nil
	case "UAC":
		return UAC, nil
	case "UGU":
		return UGU, nil
	case "UGC":
		return UGC, nil
	case "UGG":
		return UGG, nil
	case "UAA":
		return UAA, ErrStop
	case "UAG":
		return UAG, ErrStop
	case "UGA":
		return UGA, ErrStop
	default:
		return "", ErrInvalidBase
	}

}

func FromRNA(input string) ([]string, error) {
	var codon string
	var codon_count int = 0
	var output []string
	var err error
	var fromCodon string

	//get codon in RNA
	for i, letter := range input {
		codon += string(letter)
		if (i+1)%3 == 0 {
			fromCodon, err = FromCodon(codon)
			if err != nil {
				break
			} else {
				output = append(output, fromCodon)
			}
			codon = ""
			codon_count++
		}

	}

	if err == ErrStop {
		err = nil
	}

	return output, err
}
