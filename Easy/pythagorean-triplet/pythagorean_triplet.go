package pythagorean

import "math"

type Triplet [3]int

//a**2 + b**2 = c**2
//a < b < c
//Range returns a list of all Pythagorean triplets with sides in the range min to max inclusive.
func Range(min, max int) (t []Triplet) {

	for i := min; i < max; i++ {
		for j := i; j < max; j++ {
			cc := math.Pow(float64(i), 2) + math.Pow(float64(j), 2)
			c := math.Sqrt(cc)

			if c == float64(int(c)) {
				t = append(t, Triplet{i, j, int(c)})
			}
		}
	}

	return t
}

//a**2 + b**2 = c**2
//a < b < c
// a + b + c = p
//Sum returns a list of all Pythagorean triplets where the sum a+b+c (the perimeter) is equal to p.
func Sum(p int) (t []Triplet) {

	tfull := Range(1, p)

	for _, el := range tfull {
		if el[0]+el[1]+el[2] == p {
			t = append(t, el)
		}
	}

	return t
}