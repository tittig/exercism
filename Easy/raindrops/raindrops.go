package raindrops

func Convert(nb int) string {

	var output string = ""

	if nb%3 == 0 {
		output = output + "Pling"
	}
	if nb%5 == 0 {
		output = output + "Plang"
	}
	if nb%7 == 0 {
		output = output + "Plong"
	}

	if output == "" {
		output = string(nb)
	}

	return output
}
