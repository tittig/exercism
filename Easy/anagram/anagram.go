package anagram

import (
	"sort"
	"strings"
)

//this function detect if list variable contains anagram of s
func Detect(s string, list []string) []string {
	var output []string

	for _, word := range list {
		if len(word) == len(s) {
			w1 := SortString(strings.ToLower(s))
			w2 := SortString(strings.ToLower(word))

			if w1 == w2 && strings.ToLower(word) != strings.ToLower(s) {
				output = append(output, word)
			}
		}
	}

	return output
}

//function to sort string and make it comparable
func SortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return strings.Join(s, "")
}
