package logs

import (
	"fmt"
	"strings"
)

// Message extracts the message from the provided log line.
func Message(line string) string {
	m := strings.Split(line, ":")

	return strings.TrimSpace(m[1])
}

// MessageLen counts the amount of characters (runes) in the message of the log line.
func MessageLen(line string) int {

	return len([]rune(Message(line))) // or utf8.RuneCountInString(Message(line))
}

// LogLevel extracts the log level string from the provided log line.
func LogLevel(line string) string {
	s := strings.SplitAfter(line, "]")
	return strings.ToLower(strings.TrimLeft(strings.TrimRight(s[0], "]"), "["))
}

// Reformat reformats the log line in the format `message (logLevel)`.
func Reformat(line string) string {
	return fmt.Sprintf("%s (%s)", Message(line), LogLevel(line))
}
