package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
)

type Team struct {
	name    string
	w, d, l int
}

func Tally(input io.Reader, output io.Writer) error {
	scanner := bufio.NewScanner(input)
	var tournament = make(map[string]Team)

	for scanner.Scan() {
		var line string = scanner.Text()
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		game := strings.Split(line, ";")

		if len(game) != 3 {

			return errors.New("malformed game string")

		} else {
			//parted input
			team1 := game[0]
			team2 := game[1]
			result := game[2]

			//check input
			if team1 == team2 {
				return errors.New("both teams has the same name")
			}
			if !checkGameResult(result) {
				return errors.New("A game result is not one of the following: win|draw|loss")
			}

			//create teams type
			t1 := tournament[team1]
			t2 := tournament[team2]
			t1.name = team1
			t2.name = team2

			//add res

			game_analyze(&t1, &t2, result)

			tournament[team1] = t1
			tournament[team2] = t2
		}

	}

	//sort and write result
	teams := make([]Team, 0, len(tournament))
	for _, team := range tournament {
		teams = append(teams, team)
	}
	sort.Slice(teams, func(i, j int) bool {
		return (teams[i].pointsScored() > teams[j].pointsScored()) || (teams[i].pointsScored() == teams[j].pointsScored() && teams[i].name < teams[j].name)
	})

	header := []byte("Team                           | MP |  W |  D |  L |  P\n")
	output.Write(header)

	for _, team := range teams {
		output.Write([]byte(team.resultString()))
	}

	return nil
}

func game_analyze(t1, t2 *Team, g string) {
	switch g {
	case "win":
		t1.registerResult("win")
		t2.registerResult("loss")

	case "draw":
		t1.registerResult("draw")
		t2.registerResult("draw")
	case "loss":
		t1.registerResult("loss")
		t2.registerResult("win")
	}
}

func (t *Team) registerResult(r string) {
	switch r {
	case "win":
		t.w++
	case "loss":
		t.l++
	case "draw":
		t.d++
	}
}

func (t *Team) matchesPlayed() int {
	return t.w + t.l + t.d
}

func (t *Team) pointsScored() int {
	return 3*t.w + t.d
}

func (t *Team) resultString() string {
	resultFormat := "%-30v | % 2d | % 2d | % 2d | % 2d | % 2d\n"
	return fmt.Sprintf(resultFormat, t.name, t.matchesPlayed(), t.w, t.d, t.l, t.pointsScored())
}

func checkGameResult(s string) bool {
	if s == "win" || s == "loss" || s == "draw" {
		return true
	} else {
		return false
	}
}
