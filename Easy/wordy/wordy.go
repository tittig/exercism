package wordy

import (
	"fmt"
	"strconv"
	"strings"
)

var commandToFunc = map[string]func(int, int) int{
	"plus":       func(a, b int) int { return a + b },
	"minus":      func(a, b int) int { return a - b },
	"multiplied": func(a, b int) int { return a * b },
	"divided":    func(a, b int) int { return a / b },
}

func Answer(s string) (int, bool) {
	//remove trailing questionmark
	s = strings.Trim(s, "?")

	//split line
	parts := strings.Split(s, " ")
	//check input size and the question
	if len(parts) < 3 || parts[0] != "What" {
		return -1, false
	}

	//parse first value
	res, e := strconv.Atoi(parts[2])
	if e != nil {
		return -1, false
	}

	//return first value if no op
	if len(parts) == 3 {
		return res, true
	}

	//loop over operator, value pairs !
	ok := true

	fmt.Println(parts)
	for i := 3; i < len(parts); i += 2 {
		op := parts[i]

		if op == "multiplied" || op == "divided" || op == "minus" || op == "plus" {

			if op == "multiplied" || op == "divided" {
				i++
			} //"divided by, multiplied by"

			//check if there a value after the op
			if len(parts) == i+1 {
				return -1, false
			} else {
				val := parts[i+1]
				res, ok = handle(res, op, val)
			}

			if !ok {
				return -1, false
			}
		} else {
			ok = false
		}
	}

	if ok {
		return res, ok
	} else {
		return -1, ok
	}

}

func handle(a int, op, s2 string) (int, bool) {

	b, e := strconv.Atoi(s2)
	f, exists := commandToFunc[op]

	if e != nil || !exists {
		return -1, false
	}

	return f(a, b), true
}
