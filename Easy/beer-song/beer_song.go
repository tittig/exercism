package beer

import (
	"errors"
	"fmt"
)

// Maximum number of bottles permitted on wall
const max = 99

// Verse returns the specific verse of a given number.
func Verse(n int) (string, error) {

	switch {
	case n == 2:
		return "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n", nil
	case n == 1:
		return "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n", nil
	case n == 0:
		return "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n", nil
	case n > 2 && n <= max:
		return fmt.Sprintf("%d bottles of beer on the wall, %d bottles of beer.\nTake one down and pass it around, %d bottles of beer on the wall.\n", n, n, n-1), nil
	default:
		return "", errors.New("number of verse is invalid")
	}

}

func Verses(upper, lower int) (string, error) {
	var s string = ""

	//check upper and lower
	if upper <= lower {
		return "", errors.New("upper is lower than lower")
	}

	for i := upper; i >= lower; i-- {
		v, err := Verse(i)
		if err != nil {
			return "", err
		}

		s += v + "\n"
	}

	return s, nil
}

func Song() string {
	verses, _ := Verses(max, 0)

	return verses
}
