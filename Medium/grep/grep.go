package grep

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

func Search(pattern string, flags, files []string) []string {

	//create variables
	var output []string = []string{}
	var flag_n bool = CheckFlag("-n", flags)
	var flag_l bool = CheckFlag("-l", flags)
	var flag_i bool = CheckFlag("-i", flags)
	var flag_v bool = CheckFlag("-v", flags)
	var flag_x bool = CheckFlag("-x", flags)

	//check flag
	switch {
	case flag_i:
		pattern = `(?i)` + pattern
	case flag_x:
		pattern = `^` + pattern + `$`
	}
	re := regexp.MustCompile(pattern)

	//read file in files slice
	for _, filename := range files {

		//open file
		file, err := os.Open(filename)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
		defer file.Close()

		//read file
		countLine := 1
		fileScanner := bufio.NewScanner(file)
		fileScanner.Split(bufio.ScanLines)

		for fileScanner.Scan() {
			line := fileScanner.Text()

			if len(files) > 1 {
				if re.MatchString(line) != flag_v {
					if flag_l {
						output = append(output, filename)
						break
					}

					if flag_n {
						output = append(output, filename+":"+strconv.Itoa(countLine)+":"+line)
					} else {
						output = append(output, filename+":"+line)
					}
				}

			} else {

				if re.MatchString(line) != flag_v {
					if flag_l {
						output = append(output, filename)
						break
					}

					if flag_n {
						output = append(output, strconv.Itoa(countLine)+":"+line)
					} else {
						output = append(output, line)
					}
				}
			}

			countLine++
		}

	}

	return output
}

//CheckFlag checks if f is in flags list
func CheckFlag(f string, flags []string) bool {

	for _, el := range flags {
		if el == f {
			return true
		}
	}

	return false
}
