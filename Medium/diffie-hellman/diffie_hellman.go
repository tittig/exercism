package diffiehellman

import (
	"crypto/rand"
	"math/big"
)

//PrivateKey creates a random big.Int between 2 and p
func PrivateKey(p *big.Int) *big.Int {
	//create a random number between [0,p-2]
	var _two = big.NewInt(2)
	new_p := big.NewInt(0).Sub(p, _two)
	nBig, err := rand.Int(rand.Reader, new_p)

	if err != nil {
		panic(err)
	}

	//return a random number between [2,p]
	return nBig.Add(_two, nBig)

}

//PublicKey generates a big.Int = g**a mod p
func PublicKey(a *big.Int, p *big.Int, g int64) *big.Int {

	return new(big.Int).Exp(big.NewInt(g), a, p)
}

//SecretKey generate a big.Int =  B**a mod p
func SecretKey(a *big.Int, B *big.Int, p *big.Int) *big.Int {

	return new(big.Int).Exp(B, a, p)

}

// NewPair generates a new public/private keypair.
func NewPair(p *big.Int, g int64) (private *big.Int, public *big.Int) {

	private = PrivateKey(p)
	public = PublicKey(private, p, g)

	return
}
