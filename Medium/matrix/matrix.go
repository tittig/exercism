package matrix

import (
	"errors"
	"strconv"
	"strings"
	"unicode"
)

type Matrix [][]int

//New function enables to transform string input to matrix
func New(s string) (*Matrix, error) {
	//check input (have to contain only digit or space or /n)
	for _, el := range s {
		if !(unicode.IsNumber(el) || unicode.IsSpace(el) || el == '\n') {
			return nil, errors.New("string doesn't contain only number, space or line return")
		}
	}

	//split s by \n to have every row of the matrix
	array := strings.Split(s, "\n")

	//create the matrix
	m := Matrix{}
	for _, el := range array {
		row, err := SliceAtoi(strings.Split(el, " "))

		if err != nil {
			return nil, errors.New("problem to convert string to int in SliceAtoi func")
		}

		//create the final matrix
		m = append(m, row)
	}

	//check matrix format
	row_size := len(m[0])
	for _, el := range m {
		if row_size != len(el) {
			return nil, errors.New("row size issue in the matrix")
		}
	}

	return &m, nil
}

//SliceAtoi convert slice of string to slice of int
func SliceAtoi(sa []string) ([]int, error) {
	//si := make([]int, 0, len(sa))
	var si []int

	for _, a := range sa {
		if a != "" {
			i, err := strconv.Atoi(a)
			if err != nil {
				return si, err
			}
			si = append(si, i)
		}
	}

	return si, nil
}

//Rows return the list of row in the matrix
func (m *Matrix) Rows() [][]int {
	n := make([][]int, len(*m))

	for i, row := range *m {
		n[i] = make([]int, len(row))

		for j, val := range row {
			n[i][j] = val
		}
	}

	return n
}

//Cols return the list of columns in the matrix
func (m *Matrix) Cols() [][]int {
	var nCols int

	if len(*m) > 0 {
		nCols = len((*m)[0])
	}

	n := make([][]int, nCols)

	for _, row := range *m {
		for i, col := range row {
			n[i] = append(n[i], col)
		}
	}

	return n
}

func (m *Matrix) Set(row, column, value int) bool {

	if row >= 0 && column >= 0 && len(*m)-1 >= row && len((*m)[0])-1 >= column {
		(*m)[row][column] = value
		return true
	}

	return false
}
