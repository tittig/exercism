package ocr

import (
	"strings"
)

var m = map[string]string{
	`
 _ 
| |
|_|`: "0",
	`
   
  |
  |`: "1",
	`
 _ 
 _|
|_ `: "2",
	`
 _ 
 _|
 _|`: "3",
	`
   
|_|
  |`: "4",
	`
 _ 
|_ 
 _|`: "5",
	`
 _ 
|_ 
|_|`: "6",
	`
 _ 
  |
  |`: "7",
	`
 _ 
|_|
|_|`: "8",
	`
 _ 
|_|
 _|`: "9",
}

//Recognize enables recognize numbers in a string with "_" and "|" elements
//one number is specified on 4 lines and every 3 spaces is specified a digit
func Recognize(input string) (l []string) {

	//split input by \n
	split_input := strings.Split(input, "\n")
	split_input = split_input[:len(split_input)-1]

	//split split_input by numbers
	var split_final [][]string
	for i := 0; i < len(split_input)/4; i++ {
		split_final = append(split_final, split_input[0+(i*4):4+(i*4)])
	}

	for _, number := range split_final {

		var list_of_s []string
		digit_number := len(number[1]) / 3

		for i := 0; i < digit_number; i++ {

			var s string = ""

			for j, el := range number {

				if j != 0 {
					s += "\n"
					s += el[0+(i*3) : 3+(i*3)]
				}

			}

			//append a digit in the list
			list_of_s = append(list_of_s, recognizeDigit(s))
		}

		//append the number in the final list
		l = append(l, strings.Join(list_of_s[:], ""))
	}

	return l
}

//recognizeDigit enebles to recognize digit between 0 and 9 in a string with "_" and "|" elements
func recognizeDigit(input string) string {

	v, ok := m[input]

	if !ok {
		return "?"
	}

	return v
}


//Better solution :

// Package ocr provides a function for recognizing ascii art numbers.
/*
package ocr

import "strings"

var table = map[string]string{
	" _ | ||_|": "0",
	"     |  |": "1",
	" _  _||_ ": "2",
	" _  _| _|": "3",
	"   |_|  |": "4",
	" _ |_  _|": "5",
	" _ |_ |_|": "6",
	" _   |  |": "7",
	" _ |_||_|": "8",
	" _ |_| _|": "9",
}

// Recognize returns a slice of strings containing the lines of numbers detected
// in the provided input.
func Recognize(s string) []string {
          
	var o []string
	lines := strings.Split(s, "\n")

	for i := 1; i < len(lines); i += 4 {

		o = append(o, recognizeLine(lines[i:i+3]))

	}

	return o
}

func recognizeLine(line []string) string {

	var o string

	for i := 0; i < len(line[0]); i += 3 {

		o += recognizeDigit(line[0][i:i+3] + line[1][i:i+3] + line[2][i:i+3])

	}

	return o
}

        
func recognizeDigit(s string) string {

	if d, ok := table[s]; ok {
		return d
	}

	return "?"

}