package yacht

var map_OnesToSixes = map[string]int{
	"ones":   1,
	"twos":   2,
	"threes": 3,
	"fours":  4,
	"fives":  5,
	"sixes":  6,
}

//Score computes score for all category
func Score(dices []int, category string) (score int) {

	switch category {
	case "ones", "twos", "threes", "fours", "fives", "sixes":
		score = OnesToSixes(dices, category)
	case "full house":
		score = FullHouse(dices)
	case "four of a kind":
		score = FourOfKind(dices)
	case "little straight":
		score = LittleStraight(dices)
	case "big straight":
		score = BigStraight(dices)
	case "choice":
		score = Choice(dices)
	case "yacht":
		score = Yacht(dices)

	}

	return score
}

//Yacht computes the score with yatch category and dices input
func Yacht(dices []int) int {

	same := dices[0]

	for i := 1; i < len(dices); i++ {
		if same != dices[i] {
			return 0
		}
	}

	return 50
}

//OnesToSixes computes the score with OnesToSixes category and dices input
func OnesToSixes(dices []int, category string) (score int) {

	for i := 0; i < len(dices); i++ {
		if dices[i] == map_OnesToSixes[category] {
			score += map_OnesToSixes[category]
		}
	}

	return score
}

//FullHouse computes the score with full house category and dices input
func FullHouse(dices []int) (score int) {

	m := make(map[int]int)

	for _, el := range dices {
		m[el]++
	}

	if len(m) != 2 {
		return
	} else {

		for k, v := range m {
			if v == 2 || v == 3 {
				score += k * v
			} else {
				return 0
			}
		}
		return
	}

}

//FourOfKind computes the score with four of a kind category and dices input
func FourOfKind(dices []int) (sore int) {

	m := make(map[int]int)

	for _, el := range dices {
		m[el]++
	}

	if len(m) == 2 || len(m) == 1 {
		for k, v := range m {
			if v == 4 || v == 5 {
				return k * 4
			}
		}
		return
	} else {

		return
	}
}

//LittleStraight computes the score with little straight category and dices input
func LittleStraight(dices []int) int {

	m := map[int]int{
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
	}

	for _, el := range dices {
		m[el]++
	}

	for k, v := range m {
		if (k == 6 && v != 0) || (k != 6 && v != 1) {

			return 0
		}
	}

	return 30
}

//BigStraight computes the score with big straight category and dices input
func BigStraight(dices []int) int {

	m := map[int]int{
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
	}

	for _, el := range dices {
		m[el]++
	}

	for k, v := range m {
		if (k == 1 && v != 0) || (k != 1 && v != 1) {

			return 0
		}
	}

	return 30
}

//Choice computes the score with choice category and dices input
func Choice(dices []int) (score int) {

	for _, el := range dices {
		score += el
	}

	return score
}
