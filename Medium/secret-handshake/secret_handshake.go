package secret

import (
	"fmt"
)

func Handshake(code uint) []string {

	var output []string
	bin_value := fmt.Sprintf("%b", code)
	fmt.Printf("Binary value of %d is = %s\n", code, bin_value)

	//Add secret handshake
	if code&1 == 1 {
		output = append(output, "wink")
	}
	if code&2 == 2 {
		output = append(output, "double blink")
	}
	if code&4 == 4 {
		output = append(output, "close your eyes")
	}
	if code&8 == 8 {
		output = append(output, "jump")
	}

	//check if reverse order
	revOutput := make([]string, len(output))
	if code&16 == 16 {
		for i := range output {
			revOutput[len(output)-1-i] = output[i]
		}
	} else {
		revOutput = output
	}

	return revOutput
}
