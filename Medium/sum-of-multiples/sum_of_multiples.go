package summultiples

//SumMultiples compute the sum of number,below the limit, which are divisible by divisors list
func SumMultiples(limit int, divisors ...int) (sum int) {

	for i := 1; i < limit; i++ {
		for j, divisor := range divisors {
			if divisor != 0 && i%divisor == 0 && !DoesItAlreadyHaveDivider(i, divisors[:j]) {
				sum += i
			}
		}
	}

	return sum
}

//DoesItAlreadyHaveDivider check if nb has already a divisor
func DoesItAlreadyHaveDivider(nb int, divisors []int) bool {

	for _, divisor := range divisors {
		if nb%divisor == 0 && divisor != 0 {
			return true
		}
	}

	return false
}
