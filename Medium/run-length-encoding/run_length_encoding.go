package encode

import (
	"strconv"
	"strings"
	"unicode"
)

//RunLengthEncode implements run-length encoding
func RunLengthEncode(input string) (s string) {
	//check if input is empty
	if input == "" {
		return s
	}

	//encode
	count := 1
	char := input[0]

	for i := 1; i < len(input); i++ {
		if input[i] == char {
			count++
		} else {
			switch count {
			case 1:
				s += string(char)
				char = input[i]
			default:
				s += strconv.Itoa(count) + string(char)
				char = input[i]
				count = 1
			}
		}
	}

	//add last char
	switch count {
	case 1:
		s += string(input[len(input)-1])
	default:
		s += strconv.Itoa(count) + string(input[len(input)-1])
	}

	return s
}

//RunLengthDecode implements run-lenght decoding
func RunLengthDecode(input string) (s string) {
	//check if input is empty
	if input == "" {
		return s
	}

	//decode
	count := ""

	for _, el := range input {
		if unicode.IsNumber(el) {
			count += string(el)
		} else {
			switch count {
			case "":
				s += string(el)
			default:
				nb, _ := strconv.Atoi(count)
				s += strings.Repeat(string(el), nb)
				count = ""
			}
		}
	}

	return s
}
