package allergies

import (
	"sort"
)

var allergiesScore = map[string]int{
	"eggs":         1,
	"peanuts":      2,
	"shellfish":    4,
	"strawberries": 8,
	"tomatoes":     16,
	"chocolate":    32,
	"pollen":       64,
	"cats":         128,
}

type byScore []string

// create a custom sort
func (s byScore) Len() int {
	return len(s)
}
func (s byScore) Score(input string) int {
	return allergiesScore[input]
}
func (s byScore) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byScore) Less(i, j int) bool {
	return s.Score(s[i]) < s.Score(s[j])
}

//Allergies gives the list of allergies in function of score input
func Allergies(score uint) (result []string) {

	if int(score-255) > 0 {
		score = score%255 - 1
	}

	//get list of allergies
	if score%2 != 0 {
		result = append(result, "eggs")
		score -= 1
	}
	if int(score-128) >= 0 {
		result = append(result, "cats")
		score -= 128
	}
	if int(score-64) >= 0 {
		result = append(result, "pollen")
		score -= 64
	}
	if int(score-32) >= 0 {
		result = append(result, "chocolate")
		score -= 32
	}
	if int(score-16) >= 0 {
		result = append(result, "tomatoes")
		score -= 16
	}
	if int(score-8) >= 0 {
		result = append(result, "strawberries")
		score -= 8
	}
	if int(score-4) >= 0 {
		result = append(result, "shellfish")
		score -= 4
	}
	if int(score-2) >= 0 {
		result = append(result, "peanuts")
		score -= 2
	}

	//sort list of allergies
	sort.Sort(byScore(result))

	return result
}

//AllergicTo checks if you are allergic in function of the score and allergie input
func AllergicTo(score uint, input string) bool {

	allergiesList := Allergies(score)

	return Contains(allergiesList, input)
}

//Contains checks if a string is present in a slice
func Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
