package railfence

import (
	"strings"
)

//Encode encodes the rail fence cipher algorithm
func Encode(input string, n int) string {

	rails := make([]string, n)
	i, sign := 0, 1

	for _, c := range input {
		rails[i] += string(c)

		if (i+sign == n && sign == 1) || (i+sign == -1 && sign == -1) {
			sign *= -1
		}

		i += sign
	}

	return strings.Join(rails, "")
}

//Decode decodes the rail fence cipher algorithm
func Decode(s string, n int) string {

	postDist := 2*n - 2
	k, l := 0, len(s)

	decode := make([]byte, l)

	for i := 0; i < n; i++ {
		offset := postDist - 2*i // offset of next char

		for j := i; j < l; j += postDist {
			if k < l { // add a new letter every "post"
				decode[j] = s[k]
				k++
			}

			if i != 0 && i != n-1 && j+offset < l { // add a second letter if on diag
				decode[j+offset] = s[k]
				k++
			}
		}

	}

	return string(decode)

}
