package bowling

import (
	"errors"
)

//Game for one player
type Game struct {
	playerName  string  //nom du joueur
	roll        int     //nombre de lancer par tour
	frame       int     //tour de jeu de la partir
	playerScore [][]int //suivi du nombre de quille tombé par frame (0,1,2...9)
	//playerStrikeSpare map[Frame]rune  //check si il y a eu un spare(/) ou strike (x) sinon none (n)
}

func NewGame() *Game {
	g := new(Game)
	g.roll = 1
	g.frame = 0
	g.playerScore = make([][]int, 12)

	return g
}

func (g *Game) Roll(pins int) error {

	//check pins value
	if pins < 0 || pins > 10 {
		return errors.New("pins isn't between 0 and 10")
	}

	//check if the game is over
	if g.frame == 10 && len(g.playerScore[9]) != 1 && g.playerScore[9][0]+g.playerScore[9][1] != 10 {
		return errors.New("game is finish")
	}
	if g.frame == 11 && g.playerScore[10][0] != 10 {
		return errors.New("game is finish")
	}

	g.playerScore[g.frame] = append(g.playerScore[g.frame], pins)

	switch g.roll {
	case 1:
		if pins == 10 {
			//if strike go next frame
			g.frame++
		} else {
			g.roll++
		}
	case 2:
		//check score in frame
		if pins+g.playerScore[g.frame][0] > 10 {
			return errors.New("score frame is bigger than 10")
		}

		//check if it isn't the last roll
		if g.frame == 10 && len(g.playerScore[9]) != 1 {
			return errors.New("cannot roll after bonus roll for spare")
		}
		//after two rolls go next frame
		g.frame++
		g.roll = 1
	}

	return nil
}

func (g *Game) Score() (int, error) {

	score := 0

	//compute score for frame between 0 and 9
	for i := 0; i <= 9; i++ {
		if len(g.playerScore[i]) == 0 {
			return 0, errors.New("the game isn't finish")
		}

		s, err := g.ScoreForOneFrame(i)

		if err != nil {
			return 0, errors.New("the game isn't finish")
		}

		score += s
	}

	return score, nil
}

//ScoreForOneFrame computes the score for one frame between 0 and 8
func (g *Game) ScoreForOneFrame(frame int) (int, error) {

	score := 0

	switch {
	//strike case
	case len(g.playerScore[frame]) == 1:
		switch {
		case len(g.playerScore[frame+1]) == 2:
			score += 10 + g.playerScore[frame+1][0] + g.playerScore[frame+1][1]
		case len(g.playerScore[frame+1]) == 1 && len(g.playerScore[frame+2]) != 0:
			score += 10 + g.playerScore[frame+1][0] + g.playerScore[frame+2][0]
		default:
			return 0, errors.New("the game isn't finish")
		}
	//spare case
	case len(g.playerScore[frame]) == 2 && g.playerScore[frame][0]+g.playerScore[frame][1] == 10:
		if len(g.playerScore[frame+1]) > 0 {
			score += 10 + g.playerScore[frame+1][0]
		} else {
			return 0, errors.New("the game isn't finish")
		}

	//normal case
	case len(g.playerScore[frame]) == 2 && g.playerScore[frame][0]+g.playerScore[frame][1] < 10:
		score += g.playerScore[frame][0] + g.playerScore[frame][1]
	}

	return score, nil
}
