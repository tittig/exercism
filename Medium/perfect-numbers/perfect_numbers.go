package perfect

import "errors"

type Classification string

var ErrOnlyPositive error = errors.New("positive number is required")

const (
	ClassificationPerfect   Classification = "ClassificationPerfect"
	ClassificationAbundant  Classification = "ClassificationAbundant"
	ClassificationDeficient Classification = "ClassificationDeficient"
	ClassificationError     Classification = "ClassificationError"
)

func Classify(input int64) (Classification, error) {
	var sum int64 = 0
	var i int64

	for i = 1; i < input; i++ {
		if input%i == 0 {
			sum += i
		}
	}

	switch {
	case sum == input && input > 0:
		return ClassificationPerfect, nil
	case sum < input && input > 0:
		return ClassificationDeficient, nil
	case sum > input && input > 0:
		return ClassificationAbundant, nil
	default:
		return ClassificationError, ErrOnlyPositive
	}

}
