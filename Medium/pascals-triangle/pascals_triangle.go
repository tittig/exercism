package pascal

//global variable where store the result
var output [][]int

//Triangle adds in "output" variable the row with the "size" index
func Triangle(size int) [][]int {

	switch size {
	case 0:
		return nil
	case 1:
		output = append(output, []int{1})
		return output
	case 2:
		output = append(output, []int{1, 1})
		return output
	default:
		l := make([]int, size)

		for i := 0; i < size; i++ {

			switch i {

			case 0:
				l[0] = 1
			case size - 1:
				l[size-1] = 1
			default:
				l[i] = output[size-2][i-1] + output[size-2][i]
			}
		}

		output = append(output, l)

	}

	return output
}

//Recursive solution
//func Triangle(n int) [][]int {
//
//	if n == 1 {
//		return [][]int{{1}}
//	}
//
//	prevRow := Triangle(n - 1)
//	currentRow := make([]int, n)
//	currentRow[0], currentRow[n-1] = 1, 1
//
//	for i := 1; i < n-1; i++ {
//		currentRow[i] = prevRow[n-2][i-1] + prevRow[n-2][i]
//	}
//
//	return append(prevRow, currentRow)
//}
