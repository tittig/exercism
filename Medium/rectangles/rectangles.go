package rectangles

//checkRectangle returns 1 if the topleft bottomright pair form a valid rectangle
func checkRectangle(pattern []string, tl, br [2]int) int {
	//check top and bottom
	for i := tl[0]; i <= br[0]; i++ {

		if pattern[i][tl[1]] == ' ' || pattern[i][tl[1]] == '-' {

			return 0
		}

		if pattern[i][br[1]] == ' ' || pattern[i][br[1]] == '-' {

			return 0
		}
	}

	//Check left and right
	for j := tl[1]; j <= br[1]; j++ {

		if pattern[tl[0]][j] == ' ' || pattern[tl[0]][j] == '|' {

			return 0
		}

		if pattern[br[0]][j] == ' ' || pattern[br[0]][j] == '|' {

			return 0
		}
	}

	return 1
}

//findRect counts the rectangles with given top left coordinates
func findRect(pattern []string, tl [2]int) int {
	count := 0

	for i := tl[0] + 1; i < len(pattern); i++ {

		for j := tl[1] + 1; j < len(pattern[0]); j++ {

			if pattern[i][j] == '+' {

				count += checkRectangle(pattern, tl, [2]int{i, j})
			}
		}
	}

	return count
}

//Count finds the number of rectangles in a pattern.
func Count(pattern []string) int {
	//loop through the pattern
	//locate  "+"  which are not on the last column or row, remember its
	//location. Now search for any "+" which are below or right of it.
	//if one if found, check to see if forms a rectangle
	height := len(pattern)

	if height == 0 {
		return 0
	}

	width := len(pattern[0])
	count := 0

	for i := 0; i < height-1; i++ {

		for j := 0; j < width-1; j++ {

			if pattern[i][j] == '+' {

				count += findRect(pattern, [2]int{i, j})
			}
		}
	}

	return count
}
