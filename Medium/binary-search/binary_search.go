package binarysearch

import (
	"math"
)

func SearchInts(slice []int, key int) int {

	index := 0
	l := float64(len(slice))

	for l > 0 {

		l = float64(len(slice)) - 1
		middle := int(math.Round(l / 2))

		index += middle

		switch {
		case key < slice[middle]:
			slice = slice[0:middle]
			index -= middle
		case key > slice[middle]:
			slice = slice[middle:]
		default:
			return index
		}

	}

	return -1

}
