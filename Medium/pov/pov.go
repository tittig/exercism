package pov

import "fmt"

//Node type
type Node string

//Graph type
type Graph struct {
	nodeList []Node
	arcList  map[Node][]Node
}

//New create a new graph
func New() *Graph {
	g := new(Graph)

	g.arcList = make(map[Node][]Node)

	return g
}

//AddNode adds node in the nodeList of Graph
func (g *Graph) AddNode(nodeLabel string) {
	//create a new node
	var n Node = Node(nodeLabel)

	//add node
	g.nodeList = append(g.nodeList, n)

}

//AddArc adds arcs in the arcList of Graph
func (g *Graph) AddArc(from, to string) {

	g.arcList[Node(from)] = append(g.arcList[Node(from)], Node(to))

}

//ArcList transforms the g.arcList (map[Node][]Node) to slice ([]string)
func (g *Graph) ArcList() []string {
	out := make([]string, 0, len(g.arcList))

	for k, n := range g.arcList {

		for _, el := range n {
			out = append(out, fmt.Sprintf("%s -> %s", k, el))
		}

	}

	return out
}

//ChangeRoot changes the root of the graph
func (g *Graph) ChangeRoot(oldRoot, newRoot string) *Graph {

	//create a copy of g
	g_copy := new(Graph)
	g_copy.nodeList = g.nodeList
	g_copy.arcList = g.arcList

	//get the list of child from the newRoot
	lineage := g_copy.Lineage(Node(newRoot))

	//create the new arcList
	newMap := make(map[Node][]Node)
	for i, el := range lineage {
		newSlice := make([]Node, 0)

		//add the parent
		if i < len(lineage)-1 {
			newSlice = append(newSlice, lineage[i+1])
		}

		//add child != parent
		for _, child := range g_copy.arcList[el] {
			if i != 0 {
				if child != lineage[i-1] {
					newSlice = append(newSlice, child)
				}

			} else {
				newSlice = append(newSlice, child)
			}
		}

		//remove el key from g.arclist
		delete(g.arcList, el)

		newMap[el] = newSlice
	}

	//concate the newMap with the oldMap
	for key, value := range g.arcList {
		newMap[key] = value
	}

	g_copy.arcList = newMap

	return g_copy
}

//Lineages get the list of child from newRoot
func (g *Graph) Lineage(newRoot Node) (output []Node) {

	output = append(output, newRoot)

	for parent, childs := range g.arcList {
		for _, child := range childs {
			if child == newRoot {
				output = append(output, g.Lineage(parent)...)
			}
		}

	}

	return output
}

// Other solution
//package pov

//import "fmt"

// A Graph is represented entirely by its arcs. The algorithm here is
// inefficient, mostly because parentOf needs to crawl through the entire
// list of arcs. Given the size of the graphs in the tests that doesn't
// matter. As an optimization of parentOf() we could store a map from
// children to parents once and update it within ChangeRoot. Or, you know,
// implement a real graph structure.

///type Graph struct {
//
//	arcs map[string][]string
//
//}

//func New() *Graph {
//
//	return &Graph{arcs: map[string][]string{}}
//
//}

//func (g *Graph) AddNode(nodeLabel string) {
//}

//func (g *Graph) AddArc(from, to string) {
//	g.arcs[from] = append(g.arcs[from], to)
//}

//func (g *Graph) ArcList() []string {
//
//	list := []string{}
//
//	for from, tos := range g.arcs {
//
//		for _, to := range tos {
//
//			list = append(list, fmt.Sprintf("%s -> %s", from, to))
//		}
//	}
//
//	return list
//
//}

//func (g *Graph) ChangeRoot(oldRoot, newRoot string) *Graph {
//
//	parent := g.parentOf(newRoot)
//
//	if parent == "" {
//
//		return g
//
//	}
//
//	if grandparent := g.parentOf(parent); grandparent != "" {
//
//		g.ChangeRoot(grandparent, parent)
//
//	}
//
//	g.reverseArc(parent, newRoot)
//
//	return g
//
//}

//func (g *Graph) parentOf(label string) string {
//
//	for from, tos := range g.arcs {
//
//		for _, to := range tos {
//
//			if to == label {
//
//				return from
//
//			}
//		}
//	}
//
//	return ""
//
//}

//func (g *Graph) reverseArc(parent, child string) {
//
//	g.AddArc(child, parent)
//	g.removeArc(parent, child)
//
//}

//func (g *Graph) removeArc(parent, label string) {
//
//	for i := range g.arcs[parent] {
//
//		if g.arcs[parent][i] == label {
//			g.arcs[parent] = append(g.arcs[parent][0:i], g.arcs[parent][i+1:]...)
//			return
//		}
//
//	}
//
//}
