package account

import "sync"

type Account struct {
	mu     sync.RWMutex
	amount int64
	open   bool
}

func Open(initialDeposit int64) *Account {

	if initialDeposit < 0 {
		return nil
	} else {
		return &Account{amount: initialDeposit, open: true}
	}

}

//Close
func (account *Account) Close() (payout int64, ok bool) {
	account.mu.Lock()
	defer account.mu.Unlock()

	if account.open {
		account.open = false
		return account.amount, true
	} else {
		return 0, false
	}
}

//Balance
func (account *Account) Balance() (balance int64, ok bool) {
	account.mu.RLock()
	defer account.mu.RUnlock()

	if account.open {
		return account.amount, account.open
	} else {
		return 0, false
	}

}

//Deposit
func (account *Account) Deposit(amount int64) (newBalance int64, ok bool) {
	account.mu.Lock()
	defer account.mu.Unlock()

	if account.amount+amount >= 0 && account.open {
		account.amount += amount
		return account.amount, true

	} else {
		return 0, false
	}

}
