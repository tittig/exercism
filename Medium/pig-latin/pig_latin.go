package piglatin

import "strings"

func Sentence(input string) (output string) {

	input_split := strings.Split(input, " ")

	if len(input_split) == 1 {

		return EnglishToPigLatin_word(input)
	} else {
		for i, s := range input_split {
			if i != 0 {
				output += " "
			}

			output += EnglishToPigLatin_word(s)
		}

		return output
	}

}

//EnglishToPigLatin_word translates a english word to pig latin word
func EnglishToPigLatin_word(input string) string {
	//Manage word wich start with a vowel sound
	if IsVowel(input[0]) || input[0:2] == "xr" || input[0:2] == "yt" {

		return input + "ay"

	}

	//Manage word wich start with a consonant sound
	index := Get_consonant_cluster(input)

	if input[index-1] == 'q' && input[index] == 'u' {

		return input[index+1:] + input[0:index+1] + "ay"
	} else {

		return input[index:] + input[0:index] + "ay"
	}

}

//Vowel_check checks if b is a vowel
func IsVowel(b byte) bool {

	return b == 'a' || b == 'e' || b == 'i' || b == 'o' || b == 'u'
}

//Get_consonant_cluster enables to get the consonant cluster at the beginning of the word
func Get_consonant_cluster(s string) (index int) {
	index = 1

	for i := 1; i < len(s); i++ {
		if !IsVowel(s[i]) && s[i] != 'y' {
			index++
		} else {
			break
		}
	}

	return index
}
