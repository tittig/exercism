package palindrome

import (
	"errors"
)

type Product struct {
	Product        int      // palindromic, of course
	Factorizations [][2]int //list of all possible two-factor factorizations of Product, within given limits, in order
}

func Products(fmin, fmax int) (pmin, pmax Product, err error) {

	//check input
	if fmin > fmax {
		err = errors.New("fmin > fmax...")

		return pmin, pmax, err
	}

	//search palindrome
	var count int = 0
	err = errors.New("no palindromes...")
	var newNb int

	for i := fmin; i <= fmax; i++ {
		for j := i; j <= fmax; j++ {
			num := i * j

			if num < 0 {
				newNb = -num
			} else {
				newNb = num
			}

			//check if the product is a palindrome
			if checkPalindrome(newNb) {

				switch count {
				//init
				case 0:
					pmin.Product = num
					pmin.Factorizations = [][2]int{{i, j}}

					pmax.Product = num
					pmax.Factorizations = [][2]int{{i, j}}

					err = nil
					count = 1

				//other case
				default:
					//pmax
					switch {
					case num == pmax.Product:
						pmax.Factorizations = append(pmax.Factorizations, [2]int{i, j})
					case num > pmax.Product:
						pmax.Product = num
						pmax.Factorizations = [][2]int{{i, j}}
					}

					//pmin
					switch {
					case num == pmin.Product:
						pmin.Factorizations = append(pmin.Factorizations, [2]int{i, j})
					case num < pmin.Product:
						pmin.Product = num
						pmin.Factorizations = [][2]int{{i, j}}
					}

				}

			}

		}
	}

	return pmin, pmax, err
}

//checkPalindrome check if a number is a palindrome
func checkPalindrome(num int) bool {
	input_num := num
	var remainder int
	res := 0

	for num > 0 {
		remainder = num % 10
		res = (res * 10) + remainder
		num = num / 10
	}

	if input_num == res {
		return true
	} else {
		return false
	}
}
