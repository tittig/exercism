// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package proverb should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package proverb

// Proverb should have a comment documenting it.
func Proverb(rhyme []string) (output []string) {

	//check if input isn't empty
	if len(rhyme) > 0 {
		for i := 0; i < len(rhyme)-1; i++ {
			s := "For want of a " + rhyme[i] + " the " + rhyme[i+1] + " was lost."
			output = append(output, s)
		}

		//append the last rhyme
		output = append(output, "And all for the want of a "+rhyme[0]+".")

		return output
	} else {

		return output
	}

}
