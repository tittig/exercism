package linkedlist

import (
	"errors"
	"fmt"
)

var ErrEmptyList = errors.New("the list is empty")

func (n *Node) NodeString() string {

	switch {
	case n.prev == nil && n.next == nil:
		return fmt.Sprintf("prev: %v current: %d next: %v}", nil, n.Val, nil)
	case n.prev == nil && n.next != nil:
		return fmt.Sprintf("prev: %v current: %d next: %d}", nil, n.Val, n.next.Val)
	case n.prev != nil && n.next == nil:
		return fmt.Sprintf("prev: %d current: %d next: %v}", n.prev.Val, n.Val, nil)
	default:
		return fmt.Sprintf("prev: %d current: %d next: %d}", n.prev.Val, n.Val, n.next.Val)
	}

}

//Node is a struct of node
type Node struct {
	prev *Node
	next *Node
	Val  interface{}
}

//List is struct of doubly linked list of nodes
type List struct {
	nodes []*Node
}

//Next returns the next node after the current node
func (e *Node) Next() *Node {

	if e.next == nil {
		return nil
	}

	return e.next
}

//Prev returns the previous node after the current node
func (e *Node) Prev() *Node {

	if e.prev == nil {
		return nil
	}

	return e.prev
}

//NewList create the doubly linked list of nodes
func NewList(args ...interface{}) *List {

	l := new(List)

	for _, el := range args {
		l.PushBack(el)
	}

	return l
}

//Pushback create a new node with the value v and push it at the end of l
func (l *List) PushBack(v interface{}) {

	//if it is the first node of the list, just add it
	if len(l.nodes) == 0 {
		var node = new(Node)
		node.Val = v
		l.nodes = append(l.nodes, node)
	} else {
		//create the new node
		var new_node = new(Node)
		//get the last node of the list
		var last_node = l.nodes[len(l.nodes)-1]
		//add value in the new node
		new_node.Val = v
		new_node.prev = last_node
		//add the new node in the list
		l.nodes = append(l.nodes, new_node)
		//add value in the last node
		last_node.next = new_node
	}

}

//PushFront create a new node with the value v and push it at the start of l
func (l *List) PushFront(v interface{}) {

	//if it is the first node of the list, just add it
	if len(l.nodes) == 0 {
		var node = new(Node)
		node.Val = v
		l.nodes = append(l.nodes, node)
	} else {
		//create the new node
		var new_node = new(Node)
		//get the first node of the list
		var last_node = l.nodes[0]
		//add value in the new node
		new_node.Val = v
		new_node.next = last_node
		//add the new node in the list
		l.nodes = append([]*Node{new_node}, l.nodes...)
		//add value in the last node
		last_node.prev = new_node
	}

}

//Reverse reverses the l.nodes list
func (l *List) Reverse() {

	var rev_l = new(List)

	for _, el := range l.nodes {
		rev_l.PushFront(el.Val)
	}

	l.nodes = rev_l.nodes

}

//PopFront removes value at front
func (l *List) PopFront() (interface{}, error) {

	switch len(l.nodes) {
	case 0:
		return 0, ErrEmptyList
	case 1:
		remove_node := l.nodes[0].Val
		l.nodes = nil

		return remove_node, nil
	default:
		remove_node := l.nodes[0].Val
		l.nodes = l.nodes[1:]
		l.nodes[0].prev = nil

		return remove_node, nil
	}

}

//PopBack removes value at back
func (l *List) PopBack() (interface{}, error) {

	switch len(l.nodes) {
	case 0:
		return 0, ErrEmptyList
	case 1:
		remove_node := l.nodes[0].Val
		l.nodes = nil

		return remove_node, nil
	default:
		remove_node := l.nodes[len(l.nodes)-1].Val
		l.nodes = l.nodes[:len(l.nodes)-1]
		l.nodes[len(l.nodes)-1].next = nil

		return remove_node, nil
	}

}

//First gets the first element in l
func (l *List) First() *Node {

	if len(l.nodes) == 0 {
		return nil
	}

	return l.nodes[0]
}

//Last gets the last element in l
func (l *List) Last() *Node {

	if len(l.nodes) == 0 {
		return nil
	}

	return l.nodes[len(l.nodes)-1]
}
