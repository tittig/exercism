package diamond

import (
	"errors"
	"strings"
)

//Gen generates the diamond kata
func Gen(b byte) (string, error) {

	//check input
	if b < 'A' || b > 'Z' {
		return "", errors.New("out of range")
	}

	//build diamond kata
	x := int(b - 'A')
	sign := 1
	output := ""

	for i := 0; i > -1; {
		//generate line of diamond
		line := ""
		if i == 0 {
			line = strings.Repeat(" ", x-i) + string('A'+byte(i)) + strings.Repeat(" ", x-i) + "\n"
		} else {
			line = strings.Repeat(" ", x-i) + string('A'+byte(i)) + strings.Repeat(" ", 2*i-1) + string('A'+byte(i)) + strings.Repeat(" ", x-i) + "\n"
		}

		//change the sign of i
		if i == x {
			sign = -1
		}

		//add line to output
		output += line
		i += sign
	}

	return output, nil
}
