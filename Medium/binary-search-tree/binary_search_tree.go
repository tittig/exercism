package binarysearchtree

import "fmt"

type SearchTreeData struct {
	left  *SearchTreeData
	data  int
	right *SearchTreeData
}

func (n *SearchTreeData) NodeString() string {

	switch {
	case n.right == nil && n.left == nil:
		return fmt.Sprintf("left: %v data: %d right: %v}", nil, n.data, nil)
	case n.left == nil && n.right != nil:
		return fmt.Sprintf("left: %v data: %d right: %d}", nil, n.data, n.right.data)
	case n.left != nil && n.right == nil:
		return fmt.Sprintf("left: %d data: %d right: %v}", n.left.data, n.data, nil)
	default:
		return fmt.Sprintf("left: %d data: %d right: %d}", n.left.data, n.data, n.right.data)
	}

}

func Bst(nb int) *SearchTreeData {

	var STD = new(SearchTreeData)
	STD.data = nb

	return STD
}

func (s *SearchTreeData) Insert(data int) {

	var STD = new(SearchTreeData)
	STD.data = data

	switch {
	case STD.data <= s.data && s.left == nil:
		s.left = STD
	case STD.data <= s.data && s.left != nil:
		s.left.Insert(data)
	case STD.data > s.data && s.right == nil:
		s.right = STD
	case STD.data > s.data && s.right != nil:
		s.right.Insert(data)
	}

}

func (s *SearchTreeData) MapString(f func(int) string) (out []string) {

	if s.left != nil {
		out = append(out, s.left.MapString(f)...)
	}

	out = append(out, f(s.data))

	if s.right != nil {
		out = append(out, s.right.MapString(f)...)
	}

	return
}

func (s *SearchTreeData) MapInt(f func(int) int) []int {

	if s == nil {
		return []int{}
	}

	return append(append(s.left.MapInt(f), f(s.data)), s.right.MapInt(f)...)
}
