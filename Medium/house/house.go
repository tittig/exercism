package house

import (
	"fmt"
	"strings"
)

var sentence []string = []string{
	"the house that Jack built.",
	"the malt\nthat lay in ",
	"the rat\nthat ate ",
	"the cat\nthat killed ",
	"the dog\nthat worried ",
	"the cow with the crumpled horn\nthat tossed ",
	"the maiden all forlorn\nthat milked ",
	"the man all tattered and torn\nthat kissed ",
	"the priest all shaven and shorn\nthat married ",
	"the rooster that crowed in the morn\nthat woke ",
	"the farmer sowing his corn\nthat kept ",
	"the horse and the hound and the horn\nthat belonged to ",
}

func Verse(i int) string {

	return fmt.Sprintf("This is %s", makeVerse(i-1))
}

func makeVerse(i int) string {

	if i == 0 {

		return sentence[i]
	}

	return fmt.Sprintf("%s%s", sentence[i], makeVerse(i-1))
}

func Song() string {

	var verses = []string{}

	for i := 1; i < len(sentence)+1; i++ {

		verses = append(verses, Verse(i))
	}

	return strings.Join(verses, "\n\n")
}
