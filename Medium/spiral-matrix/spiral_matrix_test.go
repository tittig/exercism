package spiralmatrix

import (
	"reflect"
	"testing"
)

var testCases = []struct {
	description string
	input       int
	expected    [][]int
}{
	{
		description: "empty spiral",
		input:       0,
		expected:    [][]int{},
	},
	{
		description: "trivial spiral",
		input:       1,
		expected: [][]int{
			{1},
		},
	},
	{
		description: "spiral of size 2",
		input:       2,
		expected: [][]int{
			{1, 2},
			{4, 3},
		},
	},
	{
		description: "spiral of size 3",
		input:       3,
		expected: [][]int{
			{1, 2, 3},
			{8, 9, 4},
			{7, 6, 5},
		},
	},
	{
		description: "spiral of size 4",
		input:       4,
		expected: [][]int{
			{1, 2, 3, 4},
			{12, 13, 14, 5},
			{11, 16, 15, 6},
			{10, 9, 8, 7},
		},
	},
	{
		description: "spiral of size 8",
		input:       8,
		expected: [][]int{
			{1, 2, 3, 4, 5, 6, 7, 8},
			{28, 29, 30, 31, 32, 33, 34, 9},
			{27, 48, 49, 50, 51, 52, 35, 10},
			{26, 47, 60, 61, 62, 53, 36, 11},
			{25, 46, 59, 64, 63, 54, 37, 12},
			{24, 45, 58, 57, 56, 55, 38, 13},
			{23, 44, 43, 42, 41, 40, 39, 14},
			{22, 21, 20, 19, 18, 17, 16, 15},
		},
	},
}

func TestSpiralMatrix(t *testing.T) {
	for _, testCase := range testCases {
		matrix := SpiralMatrix(testCase.input)
		if !reflect.DeepEqual(matrix, testCase.expected) {
			t.Fatalf("FAIL: %s\n\tSpiralMatrix(%v)\nexpected: %v\ngot     : %v",
				testCase.description, testCase.input, testCase.expected, matrix)
		}
		t.Logf("PASS: %s", testCase.description)
	}
}

func BenchmarkSpiralMatrix(b *testing.B) {
	if testing.Short() {
		b.Skip("skipping benchmark in short mode.")
	}
	for i := 0; i < b.N; i++ {
		for _, testCase := range testCases {
			SpiralMatrix(testCase.input)
		}
	}
}
