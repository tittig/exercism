package spiralmatrix

func SpiralMatrix(size int) [][]int {

	//create an empty output with the good size
	var output = make([][]int, size)

	for s := 0; s < size; s++ {
		output[s] = make([]int, size)
	}

	//create the spiral matrix
	row, col := 0, 0
	top, left := 0, 0             //top and left are used like a pointer on a matrix index
	down, right := size-1, size-1 //down and right are used like a pointer on a matrix index
	dir := 0                      // 0: left to right ; 1: top to down ; 2: right to left ; 3: down to top

	for nb := 1; nb <= size*size; nb++ {

		output[row][col] = nb

		switch dir {
		case 0:
			col += 1
			if col == right {
				dir = 1
				top += 1
			}
		case 1:
			row += 1
			if row == down {
				dir = 2
				right -= 1
			}
		case 2:
			col -= 1
			if col == left {
				dir = 3
				down -= 1
			}
		case 3:
			row -= 1
			if row == top {
				dir = 0
				left += 1
			}
		}

	}

	return output
}
